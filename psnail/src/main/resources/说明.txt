1:启动程序com.psnail.Application.java-- run as -- java application

2:spring boot整合mybaits实现多数据库连接，一个主数据库master，一个从数据库cluster，两个数据库分别包含
	|--controller层：拦截页面请求
	|--service层：业务处理接口
	|--serviceImpl层：业务处理实现类
	|--dao层：整合mybaits实现数据库查询
	|--entity层：实体层，对应数据库表
	|--dto层：dao层查询的结果，全部封装在dto层，前端只和dto层交互（切记不使用entity返回前端）

3:src/main/resource
	|--mapper:封装sql语句的xml文件，分主、从两个数据库
	|--sql:存放测试数据库
	|--static:spring boot默认的静态文件夹
		|--assets:网站的前端静态资源
	|--templates:ftl模板路径
		|--ui:前端层
		|--admin:后端层
	