<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="发现源码 - 发现您需要的源码，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="搜索源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>基于struts2+spring+spring jdbc实现的代码分享网</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

     <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="${base}/assets/css/to-do.css">
    
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote.css">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote-bs3.css">
    
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/style.css" /> 
    
    <link rel="stylesheet" type="text/css" href="${base}/umeditor/themes/default/css/umeditor.css" /> 
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  	  <input type="hidden" id="d_id" value="0482ef79aa234a2e87e4261b49c11666">
  
      <#include "../user/public/index/top.ftl">
      <#include "../user/public/index/left.ftl">

	  <div class="header-fixed skin-blue">
		<aside class="content-wrapper collapse sidebarLeft">
		<div class="content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper"> 
		<div class="panel panel-white">
		<div class="panel-body"> 
		<div class="row">
			
			<#include "../user/public/demand/left.ftl">
			 
		    <div class="col-sm-9 col-md-10" id="demandContent">
		        <div class="attatchments-wrapper">
		    	  <div class="panel panel-white">
					<div class="panel-body"> 
					<h3 class="subject">
		        	<img src="${base}/assets/icon_png/xuqiu_small.png" style="margin-right:5px;margin-bottom:5px;"/>
		                                基于struts+spring+spring jdbc实现的代码分享网
		                <button type="button" class="btn btn-default pull-right" data-toggle="dropdown">
		                    <span class="star" id="guanzhu">关注</span>
		                </button> 
		            </h3>
		            <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
		            
		            <div class="content">
		                <div class="message">
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                    <ul>
		                        <li>List Item 1</li>
		                        <li>List Item 2</li>
		                        <li>List Item 3</li>
		                        <li>List Item 4</li>
		                    </ul>
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum ctetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                </div>
		        	</div> 
		        	
		        	</div>
		          </div>
		        </div>
		        
		        <div class="attatchments-wrapper">
		                    	
		        	<div class="panel panel-white border-top-orange">
		                <div class="panel-heading">
		                    <h3 class="panel-title">解决方案</h3>
		                </div>
		                <div class="panel-body">
		                    <div class="panel-group" id="accordion">
		                        
		                        <div class="panel">
		                            <div class="panel-heading">
		                                <h4 class="panel-title">
		                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		                                        <span class="badge bg-light-green squared" style="margin-right:10px;">已解决</span>#1 站长
		                                    </a>
		                                </h4>
		                            </div>
		                            <div id="collapseOne" class="panel-collapse collapse in">
		                                <div class="panel-body">
		                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		                                </div>
		                            </div>
		                        </div>
		                        
		                        <div class="panel">
		                            <div class="panel-heading">
		                                <h4 class="panel-title">
		                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		                                        <span class="badge bg-light-blue squared" style="margin-right:10px;">待验证</span>#2 站长
		                                    </a>
		                                </h4>
		                            </div>
		                            <div id="collapseThree" class="panel-collapse collapse in">
		                                <div class="panel-body">
		                                	<ul class="nav nav-pills nav-stacked attatchments">
		                                        <li>
		                                            <a href="#"> 
		                                                <span class="pull-right">8.5MB</span>
		                                                <span class="badge bg-light-blue squared">DOC</span> project53.doc
		                                            </a>
		                                        </li> 
		                                        <li>
		                                            <a href="#"> 
		                                                <span class="pull-right">238.5MB</span>
		                                                <span class="badge bg-light-green squared">ZIP</span> project53.zip
		                                            </a>
		                                        </li> 
		                                        <li>
		                                            <a href="#"> 
		                                                <span class="pull-right">12.5MB</span>
		                                                <span class="badge bg-light-orange squared">PSD</span> </i> flyer.psd
		                                            </a>
		                                        </li> 
		                                        <li>
		                                            <a href="#"> 
		                                                <span class="pull-right">1.2 GB</span>
		                                                <span class="badge bg-light-purple squared">MP4</span> </i> promo_vid.mp4
		                                            </a>
		                                        </li> 
		                                    </ul>  
		                                </div>
		                            </div>
		                        </div>
		                        
		                        <div class="panel">
		                            <div class="panel-heading">
		                                <h4 class="panel-title">
		                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		                                        <span class="badge bg-light-orange squared" style="margin-right:10px;">未解决</span>#3 站长
		                                    </a>
		                                </h4>
		                            </div>
		                            <div id="collapseTwo" class="panel-collapse collapse in">
		                                <div class="panel-body">
		                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		                                </div>
		                            </div>
		                        </div>
		                        
		                    </div>
		                </div>
		            </div>
		   	    </div>
		   	    
		   	    <#if user?exists>
			    <div class="reply" style="margin-top:20px;">
			    	<script type="text/plain" id="myAnswer"></script>
			    	<br/>
			        <a onclick="sendAnswer()" id="sendAnswerButton" class="btn btn-green reply-btn">发布新的解决方案</a>
			    </div>
		        </#if>
		        
		    </div>
			
		</div> 
		</div> 
		</div>
		</div>
		</aside>
		</div>      

      <#include "../user/public/index/modal.ftl">
  </body>
  
  <!-- js placed at the end of the document so the pages load faster -->
  
  <script src="${base}/umeditor/third-party/jquery.min.js"></script>
  <script src="${base}/umeditor/umeditor.config.js"></script>
  <script src="${base}/umeditor/umeditor.min.js"></script>
  <script src="${base}/umeditor/lang/zh-cn/zh-cn.js"></script>
  
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script src="${base}/curoAdmin/js/summernote/summernote.min.js"></script>  
  <script src="${base}/curoAdmin/js/message-item-demo.js"></script> 
  
  <script type="text/javascript">
	  //实例化编辑器
      var um = UM.getEditor('myDemand');
      var myanswer = UM.getEditor('myAnswer');
      function sendDemand() {
        var title=$("#title").val();
	  	if(title==""){
		  alert("请输入标题！");
		  $('#title').focus();
		  return false;
		}
        var arr = [];
        arr.push(UM.getEditor('myDemand').getContent());
        if(arr==""){
          alert("请输入内容！");
          UM.getEditor('myDemand').focus();
		  return false;
        }
        var content = arr.join("\n");
        var allData = {
　　　　　　　    title:title,
           content:content
　　　　　 };
        $.ajax({
           url:'${base}/user/sendDemand',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           data:JSON.stringify(allData),
           success:function(data){
           		if(data.status==1){
           			alert("发布需求成功！您的需求将会在5分钟内加入到需求列表中，请耐心等待。");
                    window.location.href = "${base}/demand.html";
           		}else{
	           		alert(data.message);
           		}
           },
           error:function(){
				alert("服务器错误！请联系站长");
           }
        })
      }
      function sendAnswer() {
        var myarr = [];
        myarr.push(UM.getEditor('myAnswer').getContent());
        if(myarr==""){
          alert("请输入内容！");
          UM.getEditor('myAnswer').focus();
		  return false;
        }
        var id = $('#d_id').val();
        var content = myarr.join("\n");
        var allData = {
　　　　　　　  id:id,
		  content:content
　　　　　　};
        $.ajax({
           url:'${base}/demand/answer',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           data:JSON.stringify(allData),
           beforeSend: function () {
		        // 禁用按钮防止重复提交，发送前响应
		        $("#sendAnswerButton").attr({ disabled: "disabled" });
                $('#sendAnswerButton').text("正在将您的解决方案通过邮件发送给需求用户。。。");
		    },
           success:function(data){
           		alert(data.message);
           		if(data.status==0){
               		$('#sendAnswerButton').removeAttr("disabled");
           			$('#sendAnswerButton').text("发布新的解决方案");
           		}else if(data.status==1){
               		window.location.href = "${base}/demand/"+id+"_new.html";
           		}
           },
           error:function(){
				alert("服务器错误！请联系站长");
				$('#sendAnswerButton').removeAttr("disabled");
           		$('#sendAnswerButton').text("发布新的解决方案");
           }
         });
      }
      $("#guanzhu").bind("click",function(){
            var id = $('#d_id').val();
            var allData = {
　　　　　　　　　　  id:id
　　　　　　　　  };
	        $.ajax({
               url:'${base}/demand/guanzhu',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==0){
	               		alert(data.message);
	               		return false;
               		}else if(data.status==1){
	               		$("#guanzhu").html("<i class='fa fa-star color-light-orange'></i>已关注");
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
        });
  </script> 
</html>