<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="发现源码 - 发现您需要的源码，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="搜索源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>基于struts2+spring+spring jdbc实现的代码分享网</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

     <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="${base}/assets/css/to-do.css">
    
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote.css">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote-bs3.css">
    
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/style.css" /> 

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <#include "public/index/top.ftl">
      <#include "public/index/left.ftl">
      <#include "public/resources/detail.ftl">
      <#include "public/index/modal.ftl">
  </body>
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script src="${base}/curoAdmin/js/summernote/summernote.min.js"></script>  
  <script src="${base}/curoAdmin/js/message-item-demo.js"></script> 
</html>