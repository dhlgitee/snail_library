<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	
	  <#include "public/index/top.ftl">
	  <div id="login-page">
	  	<div class="container">
		      <div class="form-login">
		        <h2 class="form-login-heading">注册</h2>
		        <div class="login-wrap">
		            <input type="text" name="email" id="email" class="form-control" placeholder="邮箱" autofocus>
		            <br/>
		            <button id="zhuceEmailButton" class="btn btn-theme btn-block" type="submit" onclick="settime(this)">
					<img src="${base}/assets/icon_png/youjian.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;">
					发送验证邮件</button>
		            <hr>
		            <div class="registration">
		               	 已有账号？
		                <a href="${base}/login.html">登录</a>
		            </div>
		        </div>
		      </div>	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${base}/assets/js/jquery.js"></script>
    <script src="${base}/assets/js/autoMail.1.0.min.js"></script>
    <script src="${base}/assets/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="${base}/assets/js/jquery.backstretch.min.js"></script>
    <script>
        //$.backstretch("背景图片路径", {speed: 500});
    	$(document).ready(function(){
			$('#email').autoMail({
				emails:['qq.com','163.com','126.com','sina.com','sohu.com']
			});
			$("#zhuceEmailButton").bind("click",function(){
                var email = $('#email').val();
		        if(email==""){
				    alert("邮箱不能为空！");
				    $('#email').focus();
				    return false;
				}
				if(!email.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/)){
				    alert("格式不正确！请重新输入");
				    $('#email').focus();
				    return false;
				}
				$('#zhuceEmailButton').attr("disabled", "disabled");
		        $.ajax({
	               url:'${base}/email/zhuce',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:email,
	               beforeSend: function () {
				        // 禁用按钮防止重复提交，发送前响应
				        $("#zhuceEmailButton").attr({ disabled: "disabled" });
	                    $('#zhuceEmailButton').text("邮件正在发送。。。");
				    },
	               success:function(data){
	               		alert(data.message);
	               		if(data.status==0){
	               			$('#zhuceEmailButton').removeAttr("disabled");
	               		}else if(data.status==1){
		                    $('#zhuceEmailButton').text("邮件发送成功");
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
						$('#zhuceEmailButton').removeAttr("disabled");
	               }
		         });
            });
		});
    </script>

  </body>
</html>
