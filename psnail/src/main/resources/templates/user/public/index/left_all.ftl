<#assign base=request.contextPath />
<aside>
  <div id="sidebar"  class="nav-collapse ">
      <ul class="sidebar-menu" id="nav-accordion">
      	  <p class="centered">
		  <a href="${base}/login.html"><img src="${base}/assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
      	  <h5 class="centered">尚未登录</h5>
          <li class="mt">
              <a class="active" href="${base}/index.html">
                  <img src="${base}/assets/icon_png/shouye.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>首页</span>
              </a>
          </li>
          <li class="sub-menu">
              <a href="${base}/resources.html" >
                  <img src="${base}/assets/icon_png/sousuo.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>发现源码</span>
              </a>
              <ul class="sub">
                  <li><a  href="${base}/resources.html?choose=all"><img src="${base}/assets/icon_png/quanbu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>全部源码</a></li>
                  <li><a  href="${base}/resources.html?choose=love"><img src="${base}/assets/icon_png/xihuan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>喜欢最多</a></li>
				  <li><a  href="${base}/resources.html?choose=down"><img src="${base}/assets/icon_png/xiazai.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>下载最多</a></li>
				  <li><a  href="${base}/resources.html?choose=demo"><img src="${base}/assets/icon_png/chakan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>演示最多</a></li>
              </ul>
          </li>
          <li class="sub-menu">
              <a href="${base}/needs.html" >
                  <img src="${base}/assets/icon_png/xuqiu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>发布需求</span>
              </a>
              <ul class="sub">
                  <li><a  href="needs.ftl"><img src="${base}/assets/icon_png/fabu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>网站发布</a></li>
              </ul>
          </li>
          <li class="mt">
              <a href="${base}/needs.html" >
                  <img src="${base}/assets/icon_png/dingzhi_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>私人定制</span>
                  <img src="${base}/assets/icon_png/tequan_small.png" style="margin-right:5px;margin-bottom:5px;"/>
              </a>
          </li>
          
		  <li class="sub-menu">
              <a href="${base}/needs.html" >
                  <img src="${base}/assets/icon_png/peixun.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>私人定制</span>
                  <img src="${base}/assets/icon_png/tequan.png" style="width:20px;height:15px;margin-right:5px;margin-bottom:5px;"/>
              </a>
              <ul class="sub">
                  <li><a  href="needs.ftl"><img src="${base}/assets/icon_png/dingzhi.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>需求定制</a></li>
                  <li><a  href="needs.ftl"><img src="${base}/assets/icon_png/peixun.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>指导开发</a></li>
              </ul>
          </li>
          <li class="mt">
              <a href="#" >
                  <img src="${base}/assets/icon_png/dingzhi_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>我的私人定制</span>
                  <img src="${base}/assets/icon_png/tequan_small.png" style="margin-right:5px;margin-bottom:5px;"/>
              </a>
          </li>
		  <hr/>
		  <li class="sub-menu">
              <a href="${base}/user_login" >
                  <img src="${base}/assets/icon_png/denglu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>登录</span>
              </a>
          </li>
          <li class="sub-menu">
              <a href="${base}/user_zhuce" >
                  <img src="${base}/assets/icon_png/zhuce.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>注册</span>
              </a>
          </li>
		  <hr/>
		  <li class="sub-menu">
              <a href="" >
                  <img src="${base}/assets/icon_png/yuanma.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>分享源码</span>
              </a>
          </li>
		  <li class="sub-menu">
              <a href="" >
                  <img src="${base}/assets/icon_png/zuozhe.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>申请源码作者</span>
              </a>
          </li>
		  <hr/>
		  <li class="mt">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/wenti_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>我的提问和答案</span>
              </a>
          </li>
		  <li class="mt">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/wode_small.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我的需求大厅</span>
              </a>
          </li>
          <li class="sub-menu">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/wode.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我的需求大厅</span>
              </a>
              <ul class="sub">
                  <li><a  href="calendar.html"><img src="${base}/assets/icon_png/fabu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的发布和关注</a></li>
                  <li><a  href="gallery.html"><img src="${base}/assets/icon_png/dingzhi.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的解决方案</a></li>
              </ul>
          </li>
          <li class="sub-menu">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/yuanma.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我关注的源码</span>
              </a>
              <ul class="sub">
                  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/xihuan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的喜欢</a></li>
				  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/xiazai.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的下载</a></li>
				  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/chakan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的演示</a></li>
              </ul>
          </li>
          <li class="sub-menu">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/sikao.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我的问题和答案</span>
              </a>
              <ul class="sub">
                  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/wenti.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的提问</a></li>
				  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/daan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的答案</a></li>
              </ul>
          </li>
		  <li class="sub-menu">
              <a href="#" >
                  <img src="${base}/assets/icon_png/peixun.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我的私人定制</span>
                  <img src="${base}/assets/icon_png/tequan.png" style="width:20px;height:15px;margin-right:5px;margin-bottom:5px;"/>
              </a>
              <ul class="sub">
                  <li><a  href="needs.ftl"><img src="${base}/assets/icon_png/dingzhi.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的需求定制</a></li>
                  <li><a  href="needs.ftl"><img src="${base}/assets/icon_png/peixun.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>我的指导开发</a></li>
              </ul>
          </li>
		  <hr/>
		  <li class="sub-menu">
              <a href="javascript:;" >
                  <img src="${base}/assets/icon_png/zhanghao1.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>我的账户</span>
              </a>
              <ul class="sub">
                  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/xiugaimima.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>修改密码</a></li>
				  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/xiugaitouxiang.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>修改头像</a></li>
				  <li><a  href="buttons.html"><img src="${base}/assets/icon_png/tuichu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>退出账号</a></li>
              </ul>
          </li>
      </ul>
  </div>
</aside>