<section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	注意事项：
                    <div class="content">
                    	1：请先登录，只有登录后才可以操作
                    	<br/>2：登录后为本次服务评分，您可以在1-5分进行选择
                    	<br/>3：您的评分非常重要，您的评分将直接关系到作者的酬劳
                    	<br/>4：若您对本次服务有不满之处，请联系站长  <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
		   	   		<input type="hidden" id="id" value="${RequestParameters["id"]}"/>
		   	   		您申请的服务：￥<span id="fuwu"></span>
		   	   		<br/><br/>源码项目：<span id="resource"></span>
		   	   		<br/><br/>
		   	   		<#if user?exists>
		   	   		<div class="demo">
					   <div id="function-demo" class="target-demo"></div>
					   <div id="function-hint" class="hint"></div>
					</div>
					</#if> 
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
</section>