<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>创建账号  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <#include "public/index/top.ftl">
  <div id="login-page">
  	<div class="container">
      <div class="form-login">
        <h2 class="form-login-heading">注册</h2>
        <div class="login-wrap">
            <input type="hidden" class="form-control" name="email" id="email" value="${RequestParameters["email"]}" placeholder="邮箱" disabled>
            <input type="text" class="form-control" name="name" id="name" placeholder="昵称" autofocus>
            <br>
            <input type="password" name="password" id="password" class="form-control" placeholder="密码">
            <br>
            <input type="password" name="password_confirm" id="password_confirm" class="form-control" placeholder="确认密码">
            <br>
            <button id="createButton" class="btn btn-theme btn-block" type="submit"><img src="${base}/assets/icon_png/zhuce.png" style="width:15px;height:15px;margin-right:5px;">注册</button>
            <hr>
            
            <div class="registration">
               	更换邮箱？
                <a href="${base}/zhuce.html">去更换</a>
            </div>

        </div>
      </div>
  	</div>
  </div>
	  
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${base}/assets/js/jquery.js"></script>
    <script src="${base}/assets/js/bootstrap.min.js"></script>
    <script src="${base}/assets/js/autoMail.1.0.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="${base}/assets/js/jquery.backstretch.min.js"></script>
    <script>
        //$.backstretch("背景图片路径", {speed: 500});
        $(document).ready(function(){
			$("#createButton").bind("click",function(){
                var email = $('#email').val();
                var name = $('#name').val();
                var password = $('#password').val();
                var password_confirm = $('#password_confirm').val();
		        if(email==""){
				    alert("邮箱不能为空！");
				    $('#email').focus();
				    return false;
				}
				if(name==""){
					alert("昵称不能为空！");
				    $('#name').focus();
				    return false;
				}else if(name.length<3||name.length>20){
					alert("昵称应该为3-20位之间");
				    $('#name').focus();
				    return false;
				}
				if(password==""){
					alert("密码不能为空！");
				    $('#password').focus();
				    return false;
				}else if(password.length<6||password.length>20){
					alert("密码应该为6-20位之间");
				    $('#password').focus();
				    return false;
				}
				if(password_confirm==""){
					alert("确认密码不能为空！");
				    $('#password_confirm').focus();
				    return false;
				}else if(password!=password_confirm){
					alert("两次密码输入不一致！");
				    $('#password_confirm').focus();
				    return false;
				}
				$('#createButton').attr("disabled", "disabled");
				var allData = {
	　　　　　　　　　　  email:email,
               		name:name,
               		password:password,
               		password_confirm:password_confirm
	　　　　　　　　};
		        $.ajax({
	               url:'${base}/user/create',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               beforeSend: function () {
				        // 禁用按钮防止重复提交，发送前响应
				        $("#createButton").attr({ disabled: "disabled" });
	                    $('#createButton').text("正在验证信息。。。");
				    },
	               success:function(data){
	               		alert(data.message);
	               		if(data.status==0){
	               			$('#createButton').removeAttr("disabled");
		                    $('#createButton').text(data.message);
	               		}else if(data.status==1){
		                    window.location.href = "${base}/login.html";
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
						$('#createButton').removeAttr("disabled");
	               }
		         });
            });
		});
    </script>

  </body>
</html>
