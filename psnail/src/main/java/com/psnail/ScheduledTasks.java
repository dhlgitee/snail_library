package com.psnail;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.psnail.master.entity.User;
import com.psnail.master.entity.UserExample;
import com.psnail.master.entity.UserExample.Criteria;
import com.psnail.master.mapper.UserMapper;
import com.psnail.master.mapper.dao.CostDao;
import com.psnail.master.mapper.dao.DemandDao;
import com.psnail.master.mapper.dao.FocusRDao;
import com.psnail.master.mapper.dao.ResourceDao;
import com.psnail.master.mapper.dao.UserDao;
import com.psnail.tools.Data2Html;
import com.psnail.tools.PathTools;
import com.psnail.tools.TextFileTools;

@Component
public class ScheduledTasks {

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private ResourceDao resourceDao;
	@Autowired
	private DemandDao demandDao;
	@Autowired
	private CostDao costDao;
	@Autowired
	private FocusRDao focusRDao;

	// 首页，每隔1分钟查询一次在线演示、免费下载、和注册的用户数量
	@Scheduled(cron = "0 */1 *  * * * ")
	public void indexNumber1() {
		// 修改注册的用户数量
		TextFileTools
				.write(ScheduledTasks.getIndexRightMainFilePath()
						+ "userNumber.ftl", false,
						String.valueOf(userDao.countByAll()));
		// 修改在线演示
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "yanshi.ftl", false, String.valueOf(Data2Html
				.indexYanShi(focusRDao.selectByWayTop(3, 5))));
		// 修改免费下载
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "xiazai.ftl", false, String.valueOf(Data2Html
				.indexXiaZai(focusRDao.selectByWayTop(2, 6))));
		UserExample vipexample = new UserExample();
		Criteria vipcriteria = vipexample.createCriteria();
		vipcriteria.andVipEqualTo(1);
		vipexample.setOrderByClause("time desc");
		List<User> vips = userMapper.selectByExample(vipexample);
		// 更新vip列表
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "vip.ftl", false, Data2Html.allVipUsers(vips));
	}

	// 首页，每隔5分钟更新一次作者数量、演示源码数量、打赏金额
	@Scheduled(cron = "0 */5 *  * * * ")
	public void indexNumber2() {
		// 修改源码作者数量
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "authorNumber.ftl", false,
				String.valueOf(userDao.countByAuthor()));

	}

	// 首页，每隔10分钟更新一次赞助记录
	@Scheduled(cron = "0 */10 *  * * * ")
	public void zanzhus() {
		UserExample example = new UserExample();
		example.setOrderByClause("time desc");
		List<User> users = userMapper.selectByExample(example);
		UserExample authorexample = new UserExample();
		authorexample.setOrderByClause("time desc");
		Criteria authorcriteria = authorexample.createCriteria();
		authorcriteria.andAuthorEqualTo(1);
		List<User> authors = userMapper.selectByExample(authorexample);
		// 更新注册用户列表
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "user.ftl", false, Data2Html.allUsers(users));
		// 更新源码作者列表
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "author.ftl", false, Data2Html.allUsers(authors));
		// 更新赞助列表
		TextFileTools.write(ScheduledTasks.getIndexTopMoneyFilePath()
				+ "zanzhu.ftl", false,
				Data2Html.allCostList(costDao.getCostListByWay3()));
		// 修改打赏金额数量
		TextFileTools
				.write(ScheduledTasks.getIndexRightMainFilePath()
						+ "moneyNumber.ftl", false,
						String.valueOf(costDao.sumByWay(3)));
	}

	// 每隔5分钟更新需求列表
	@Scheduled(cron = "0 */5 *  * * * ")
	public void demands() {
		// 修改发布需求全部需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "mainDemands.ftl", false,
				Data2Html.allDemandListToHtml(demandDao.getAllDemands(), 1));
		// 修改发布"待解决"需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "waitDemands.ftl", false, Data2Html.allDemandListToHtml(
				demandDao.getAllDemandsByState(1), 2));
		// 修改发布"待验证"需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "validateDemands.ftl", false, Data2Html.allDemandListToHtml(
				demandDao.getAllDemandsByState(2), 3));
		// 修改发布"已解决"需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "completeDemands.ftl", false, Data2Html.allDemandListToHtml(
				demandDao.getAllDemandsByState(3), 4));

		// 修改发布"VIP用户"需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "vipDemands.ftl", false, Data2Html.allDemandListToHtml(
				demandDao.getAllDemandsByVip(), 5));

		// 修改发布"赞助用户"需求列表
		TextFileTools.write(ScheduledTasks.getDemandMainFilePath()
				+ "supportDemands.ftl", false, Data2Html.allDemandListToHtml(
				demandDao.getAllDemandsByZanZhu(), 6));

		// 修改源码需求数量
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "demandNumber.ftl", false,
				String.valueOf(demandDao.countByAll()));
	}

	// 首页，每隔5分钟，更新发现源码列表
	@Scheduled(cron = "0 */5 *  * * * ")
	public void resources() {
		// 修改发现源码全部源码列表
		TextFileTools.write(ScheduledTasks.getResourcesMainFilePath()
				+ "allResources.ftl", false,
				Data2Html.allResourceListToHtml(resourceDao.getAllResources()));
		// 修改喜欢最多源码列表
		TextFileTools
				.write(ScheduledTasks.getResourcesMainFilePath()
						+ "likeResources.ftl",
						false,
						Data2Html.otherResourceListToHtml(
								resourceDao.getOtherResources(1), 1));
		// 修改下载最多源码列表
		TextFileTools
				.write(ScheduledTasks.getResourcesMainFilePath()
						+ "downResources.ftl",
						false,
						Data2Html.otherResourceListToHtml(
								resourceDao.getOtherResources(2), 2));
		// 修改演示最多源码列表
		TextFileTools
				.write(ScheduledTasks.getResourcesMainFilePath()
						+ "showResources.ftl",
						false,
						Data2Html.otherResourceListToHtml(
								resourceDao.getOtherResources(3), 3));
		// 修改演示源码数量
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "resourceNumber.ftl", false,
				String.valueOf(resourceDao.countByAll()));
		// 修改首页最新源码
		TextFileTools.write(ScheduledTasks.getIndexRightMainFilePath()
				+ "yuanma.ftl", false,
				String.valueOf(Data2Html.indexYuanMa(resourceDao.getTop(12))));

		// 修改首页源码标签
		TextFileTools
				.write(ScheduledTasks.getIndexRightMainFilePath()
						+ "biaoqian.ftl", false, String.valueOf(Data2Html
						.indexBiaoQian(resourceDao.getTop(50))));
	}

	// 获取首页右侧主要数据文件地址
	public static String getIndexRightMainFilePath() {
		return PathTools.getPath() + File.separator + "templates"
				+ File.separator + "user" + File.separator + "public"
				+ File.separator + "index" + File.separator + "right"
				+ File.separator + "main" + File.separator;
	}

	// 获取首页右侧主要数据文件地址
	public static String getIndexTopMoneyFilePath() {
		return PathTools.getPath() + File.separator + "templates"
				+ File.separator + "user" + File.separator + "public"
				+ File.separator + "index" + File.separator + "top"
				+ File.separator + "money" + File.separator;
	}

	// 获取发现源码数据文件地址
	public static String getResourcesMainFilePath() {
		return PathTools.getPath() + File.separator + "templates"
				+ File.separator + "user" + File.separator + "public"
				+ File.separator + "resources" + File.separator + "main"
				+ File.separator;
	}

	// 获取发布需求列表文件地址
	public static String getDemandMainFilePath() {
		return PathTools.getPath() + File.separator + "templates"
				+ File.separator + "user" + File.separator + "public"
				+ File.separator + "demand" + File.separator + "main"
				+ File.separator;
	}
}
