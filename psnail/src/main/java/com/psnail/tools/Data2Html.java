package com.psnail.tools;

import java.text.SimpleDateFormat;
import java.util.List;

import com.psnail.ScheduledTasks;
import com.psnail.master.entity.Answer;
import com.psnail.master.entity.Demand;
import com.psnail.master.entity.Question;
import com.psnail.master.entity.Resource;
import com.psnail.master.entity.User;
import com.psnail.master.entity.dto.AllResources;
import com.psnail.master.entity.dto.BiaoQian;
import com.psnail.master.entity.dto.CostList;
import com.psnail.master.entity.dto.Demands;
import com.psnail.master.entity.dto.FocusR;
import com.psnail.master.entity.dto.OtherResources;

public class Data2Html {

	// 标签颜色循环
	private static final String[] labelRGB = { "label label-light-blue", "label label-light-green",
			"label label-light-purple" };
	// 答案评价内容
	private static final String[] answerStar = { "待评价", "非常不好的技术解答", "很不好的技术解答", "一般的技术解答", "很好的技术解答", "非常好的技术解答" };
	//源码标签颜色
	private static final String[] resourceClass={"tagc1","tagc2","tagc5"};
	public static String allResourceListToHtml(List<AllResources> rus) {
		int i = 0;
		String result = "";
		int j=1;
		for (AllResources ru : rus) {
			result+="<tr>" + System.getProperty("line.separator");
			result+="	<td>"+j+"</td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><img src=\"${base}/user/"+ru.getPhoto()+"\" style=\"height: 45px;width: 45px;border-radius: 50%;margin: 0 10px;vertical-align: middle;\">" + System.getProperty("line.separator");
			result+="	<span class=\"name\">"+ru.getName()+"</span></a></td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><span class='" + labelRGB[i] + "'>" + ru.getLabel() + "</span></a></td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><span class=\"subject\">"+ru.getTitle()+"</span></a></td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\">"+TimeTools.getTimeFormatText(ru.getTime())+"</a></td>" + System.getProperty("line.separator");
			result+="</tr>" + System.getProperty("line.separator");
			i++;
			j++;
			if (i > 2) {
				i = 0;
			}
		}
		return result;
	}

	public static String otherResourceListToHtml(List<OtherResources> rus, int way) {
		int i = 0;int j=1;
		String other = " ";
		if (way == 1)
			other += "<img src='${base}/assets/icon_png/xihuan.png' style='width:15px;height:15px;margin-right:5px;margin-bottom:5px;'/>";
		if (way == 2)
			other += "<img src='${base}/assets/icon_png/xiazai.png' style='width:15px;height:15px;margin-right:5px;margin-bottom:5px;'/>";
		if (way == 3)
			other += "<img src='${base}/assets/icon_png/chakan.png' style='width:15px;height:15px;margin-right:5px;margin-bottom:5px;'>";
		String result = "";
		for (OtherResources ru : rus) {
			result+="<tr>" + System.getProperty("line.separator");
			result+="	<td>"+j+"</td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><img src=\"${base}/user/"+ru.getPhoto()+"\" style=\"height: 45px;width: 45px;border-radius: 50%;margin: 0 10px;vertical-align: middle;\">" + System.getProperty("line.separator");
			result+="	<span class=\"name\">"+ru.getName()+"</span></a></td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><span class='" + labelRGB[i] + "'>" + ru.getLabel() + "</span></a></td>" + System.getProperty("line.separator");
			result+="    <td><a href=\"${base}/resource/"+ru.getId()+".html\"><span class=\"subject\">"+ru.getTitle()+"</span></a></td>" + System.getProperty("line.separator");
			result += "  <td><a href=\"${base}/resource/"+ru.getId()+".html\"><span class='timestamp'>" + ru.getNumber() + other + "</span></a></td>";
			result+="</tr>" + System.getProperty("line.separator");
			i++;j++;
			if (i > 2) {
				i = 0;
			}
		}
		return result;
	}

	public static String resourceDetailToHtml(Resource r, List<Question> qs, String zixunMoney, String answerMoney,
			String fuwu2Money, String fuwu3Money, String fuwu4Money) {
		String html = "";
		html += "<#assign base=request.contextPath />" + System.getProperty("line.separator");
		html += "<!DOCTYPE html>" + System.getProperty("line.separator");
		html += "<html>" + System.getProperty("line.separator");
		html += "  <head>" + System.getProperty("line.separator");
		html += "    <meta charset=\"utf-8\">" + System.getProperty("line.separator");
		html += "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
				+ System.getProperty("line.separator");
		html += "    <meta name=\"description\" content=\"" + r.getTitle() + "\">"
				+ System.getProperty("line.separator");
		html += "    <meta name=\"author\" content=\"蜗牛库\">" + System.getProperty("line.separator");
		html += "    <meta name=\"keyword\" content=\"" + r.getTitle() + "," + r.getLabel() + "\">"
				+ System.getProperty("line.separator");
		html += "" + System.getProperty("line.separator");
		html += "    <title>" + r.getTitle() + "</title>" + System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/img/logo.ico\" rel=\"shortcut icon\">"
				+ System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/css/bootstrap.css\" rel=\"stylesheet\">"
				+ System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />"
				+ System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/css/style.css\" rel=\"stylesheet\">"
				+ System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/css/style-responsive.css\" rel=\"stylesheet\">"
				+ System.getProperty("line.separator");
		html += "    <link rel=\"stylesheet\" href=\"${base}/assets/css/to-do.css\">"
				+ System.getProperty("line.separator");
		html += "    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/summernote/summernote.css\">"
				+ System.getProperty("line.separator");
		html += "    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/summernote/summernote-bs3.css\">"
				+ System.getProperty("line.separator");
		html += "    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/style.css\" /> "
				+ System.getProperty("line.separator");
		html += "    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/umeditor/themes/default/css/umeditor.css\" />"
				+ System.getProperty("line.separator");
		html += "    " + System.getProperty("line.separator");
		html += "    <link href=\"${base}/assets/css/bootstrap-grid.min.css\" rel=\"stylesheet\">"
				+ System.getProperty("line.separator");
		html += "	 " + System.getProperty("line.separator");
		html += "    <!--[if lt IE 9]>" + System.getProperty("line.separator");
		html += "      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>"
				+ System.getProperty("line.separator");
		html += "      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>"
				+ System.getProperty("line.separator");
		html += "    <![endif]-->" + System.getProperty("line.separator");
		html += "  </head>" + System.getProperty("line.separator");
		html += "" + System.getProperty("line.separator");
		html += "  <body>" + System.getProperty("line.separator");
		html += "  	  <input type=\"hidden\" id=\"r_id\" value=\"" + r.getId() + "\">"
				+ System.getProperty("line.separator");
		html += "" + System.getProperty("line.separator");
		html += "      <#include \"../user/public/index/top.ftl\">" + System.getProperty("line.separator");
		html += "      <#include \"../user/public/index/left.ftl\">" + System.getProperty("line.separator");
		html += "      " + System.getProperty("line.separator");
		html += "      <div class=\"header-fixed skin-blue\">" + System.getProperty("line.separator");
		html += "		<aside class=\"content-wrapper collapse sidebarLeft\">" + System.getProperty("line.separator");
		html += "		<div class=\"content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper\"> "
				+ System.getProperty("line.separator");
		html += "		<div class=\"panel panel-white\">" + System.getProperty("line.separator");
		html += "		<div class=\"panel-body\"> " + System.getProperty("line.separator");
		html += "		<div class=\"row\">" + System.getProperty("line.separator");
		html += "		    <div class=\"col-sm-3 col-md-2 mg-btm-30\">" + System.getProperty("line.separator");
		html += "		    	<a id=\"show\" class=\"btn btn-blue btn-sm btn-block\">在线演示</a>"
				+ System.getProperty("line.separator");
		html += "		        <a id=\"down\" class=\"btn btn-green btn-sm btn-block\">免费下载</a>"
				+ System.getProperty("line.separator");
		html += "		        <div id=\"downUrl\"></div>" + System.getProperty("line.separator");
		/*html += "		        <#if user?exists>" + System.getProperty("line.separator");
		html += "			        <hr style=\"margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;\"/>"
				+ System.getProperty("line.separator");
		html += "		        	<input type=\"hidden\" id=\"u_id\" value=\"${user.id}\">"
				+ System.getProperty("line.separator");
		html += "			        <a onclick=\"$('#answer').hide();$('#bushu').hide();$('#question').show();\" class=\"btn btn-danger btn-sm btn-block\">技术咨询</a>"
				+ System.getProperty("line.separator");
		html += "			        <a onclick=\"$('#answer').hide();$('#bushu').show();$('#question').hide();\" class=\"btn btn-purple btn-sm btn-block\">远程部署</a>"
				+ System.getProperty("line.separator");
		html += "		        </#if>" + System.getProperty("line.separator");*/
		html += "		    	<#include \"../user/public/resources/main/soft.ftl\">"
				+ System.getProperty("line.separator");
		html += "		    </div>" + System.getProperty("line.separator");
		html += "		    " + System.getProperty("line.separator");
		html += "		    <div class=\"col-sm-9 col-md-10\">" + System.getProperty("line.separator");
		html += "		    	 <#if user?exists>" + System.getProperty("line.separator");
		html += "	        	 <div class=\"reply\" style=\"margin:20px 0;display:none;\" id=\"question\">"
				+ System.getProperty("line.separator");
		html += "			    	<div class=\"form-group\">" + System.getProperty("line.separator");
		html += "				   	   <div class=\"alert alert-theme alert-success fade in\">"
				+ System.getProperty("line.separator");
		html += "		                    <div class=\"left\">" + System.getProperty("line.separator");
		html += "		                    	<i class=\"fa fa-check icon\"></i>"
				+ System.getProperty("line.separator");
		html += "		                    </div> " + System.getProperty("line.separator");
		html += "		                    <div class=\"content\">" + System.getProperty("line.separator");
		html += "		                    	您是" + System.getProperty("line.separator");
		html += "			                    <#if (user.vip==1)><strong>VIP用户</strong>！"
				+ System.getProperty("line.separator");
		html += "								<#elseif (user.zanzhu==1)><strong>赞助用户</strong>！ "
				+ System.getProperty("line.separator");
		html += "								<#else><strong>普通用户</strong>！" + System.getProperty("line.separator");
		html += "								</#if> " + System.getProperty("line.separator");
		html += "		                    	请在下方编辑器中 <strong>详细描述</strong> 您的问题。"
				+ System.getProperty("line.separator");
		html += "		                    </div>" + System.getProperty("line.separator");
		html += "		                    <div class=\"right\">" + System.getProperty("line.separator");
		html += "		                       	了解：加入VIP群，所有会员全天活跃讨论技术问题。" + System.getProperty("line.separator");
		html += "		                    </div>" + System.getProperty("line.separator");
		html += "		               </div>" + System.getProperty("line.separator");
		html += "		            </div>" + System.getProperty("line.separator");
		html += "			    	<script type=\"text/plain\" id=\"myQuestion\"></script>"
				+ System.getProperty("line.separator");
		html += "			    	<br/>" + System.getProperty("line.separator");
		html += "			    	<button id=\"sendQuestionButton\" class=\"btn btn-green btn-sm\" onclick=\"sendQuestion()\">￥"
				+ zixunMoney + " 咨询</button>" + System.getProperty("line.separator");
		html += "	           		<button id=\"sendQuestionCancel\" class=\"btn btn-default btn-sm\" onclick=\"$('#question').hide();$('#bushu').hide();$('#answer').show();\"> 取消</button>"
				+ System.getProperty("line.separator");
		html += "			    </div>" + System.getProperty("line.separator");
		html += "			    <div class=\"attatchments-wrapper\" style=\"margin:20px 0;display:none;\" id=\"bushu\">"
				+ System.getProperty("line.separator");
		html += "		        	<div class=\"panel panel-white\">" + System.getProperty("line.separator");
		html += "						<div class=\"panel-body\"> " + System.getProperty("line.separator");
		html += "							<div class=\"reply\">" + System.getProperty("line.separator");
		html += "						    	<div class=\"form-group\">" + System.getProperty("line.separator");
		html += "							   	   <div class=\"alert alert-theme alert-success fade in\">"
				+ System.getProperty("line.separator");
		html += "					                    <img src=\"${base}/assets/icon_png/shangke.png\" style=\"margin-right:5px;\"/>"
				+ System.getProperty("line.separator");
		html += "				           				" + r.getTitle() + System.getProperty("line.separator");
		html += "					                    <div class=\"right\">" + System.getProperty("line.separator");
		html += "					                       	了解：远程部署服务，仅VIP用户、赞助用户拥有权限。"
				+ System.getProperty("line.separator");
		html += "					                    </div>" + System.getProperty("line.separator");
		html += "					               </div>" + System.getProperty("line.separator");
		html += "					            </div>" + System.getProperty("line.separator");
		html += "						    </div>" + System.getProperty("line.separator");
		html += "				            <hr style=\"margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;\"/>"
				+ System.getProperty("line.separator");
		html += "				            <div class=\"content\">" + System.getProperty("line.separator");
		html += "				                <div class=\"col-md-4 col-sm-6\">"
				+ System.getProperty("line.separator");
		html += "								    <div class=\"pricingTable\">"
				+ System.getProperty("line.separator");
		html += "								        <h3 class=\"title\">初级部署</h3>"
				+ System.getProperty("line.separator");
		html += "								        <div class=\"price-value\">￥" + fuwu2Money
				+ System.getProperty("line.separator");
		html += "								            <span class=\"month\">30-60分钟</span>"
				+ System.getProperty("line.separator");
		html += "								        </div>" + System.getProperty("line.separator");
		html += "								        <ul class=\"pricing-content\">"
				+ System.getProperty("line.separator");
		html += "								            <li>代码部署</li>" + System.getProperty("line.separator");
		html += "								            <li>30-60分钟问题答疑</li>"
				+ System.getProperty("line.separator");
		html += "								            <li>适合基础不错的初学者</li>" + System.getProperty("line.separator");
		html += "								        </ul>" + System.getProperty("line.separator");
		html += "				                        <input id=\"qq2\" name=\"qq2\" class=\"form-control\" type=\"text\" value=\"\" placeholder=\"您的QQ号码\" style=\"margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;\">"
				+ System.getProperty("line.separator");
		html += "								        <a id=\"fuwu2\" class=\"pricingTable-signup red\"><span id=\"span2\">申请</span></a>"
				+ System.getProperty("line.separator");
		html += "								    </div>" + System.getProperty("line.separator");
		html += "								</div>" + System.getProperty("line.separator");
		html += "								" + System.getProperty("line.separator");
		html += "								<div class=\"col-md-4 col-sm-6\">"
				+ System.getProperty("line.separator");
		html += "								    <div class=\"pricingTable\">"
				+ System.getProperty("line.separator");
		html += "								        <h3 class=\"title\">中级部署</h3>"
				+ System.getProperty("line.separator");
		html += "								        <div class=\"price-value\">￥" + fuwu3Money
				+ System.getProperty("line.separator");
		html += "								            <span class=\"month\">1-4小时</span>"
				+ System.getProperty("line.separator");
		html += "								        </div>" + System.getProperty("line.separator");
		html += "								        <ul class=\"pricing-content\">"
				+ System.getProperty("line.separator");
		html += "								            <li>代码部署 + 项目简单讲解</li>"
				+ System.getProperty("line.separator");
		html += "								            <li>1-4小时问题答疑</li>" + System.getProperty("line.separator");
		html += "								            <li>适合基础一般的初学者</li>" + System.getProperty("line.separator");
		html += "								        </ul>" + System.getProperty("line.separator");
		html += "								        <input id=\"qq3\" name=\"qq3\" class=\"form-control\" type=\"text\" value=\"\" placeholder=\"您的QQ号码\" style=\"margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;\">"
				+ System.getProperty("line.separator");
		html += "								        <a id=\"fuwu3\" class=\"pricingTable-signup red\"><span id=\"span3\">申请</span></a>"
				+ System.getProperty("line.separator");
		html += "								    </div>" + System.getProperty("line.separator");
		html += "								</div>" + System.getProperty("line.separator");
		html += "								" + System.getProperty("line.separator");
		html += "								<div class=\"col-md-4 col-sm-6\">"
				+ System.getProperty("line.separator");
		html += "								    <div class=\"pricingTable\">"
				+ System.getProperty("line.separator");
		html += "								        <h3 class=\"title\">高级部署</h3>"
				+ System.getProperty("line.separator");
		html += "								        <div class=\"price-value\">￥" + fuwu4Money
				+ System.getProperty("line.separator");
		html += "								            <span class=\"month\">4-8小时</span>"
				+ System.getProperty("line.separator");
		html += "								        </div>" + System.getProperty("line.separator");
		html += "								        <ul class=\"pricing-content\">"
				+ System.getProperty("line.separator");
		html += "								            <li>代码部署 + 项目全面讲解</li>"
				+ System.getProperty("line.separator");
		html += "								            <li>4-8小时问题答疑</li>" + System.getProperty("line.separator");
		html += "								            <li>适合没有基础的小白</li>" + System.getProperty("line.separator");
		html += "								        </ul>" + System.getProperty("line.separator");
		html += "								        <input id=\"qq4\" name=\"qq4\" class=\"form-control\" type=\"text\" value=\"\" placeholder=\"您的QQ号码\" style=\"margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;\">"
				+ System.getProperty("line.separator");
		html += "								        <a id=\"fuwu4\" class=\"pricingTable-signup red\"><span id=\"span4\">申请</span></a>"
				+ System.getProperty("line.separator");
		html += "								    </div>" + System.getProperty("line.separator");
		html += "								</div>" + System.getProperty("line.separator");
		html += "				            </div>" + System.getProperty("line.separator");
		html += "				        </div>" + System.getProperty("line.separator");
		html += "				    </div>" + System.getProperty("line.separator");
		html += "				</div>" + System.getProperty("line.separator");
		html += "				</#if>" + System.getProperty("line.separator");
		html += "				" + System.getProperty("line.separator");
		html += "		        <div class=\"attatchments-wrapper\" id=\"answer\">"
				+ System.getProperty("line.separator");
		html += "		        	<div class=\"panel panel-white\">" + System.getProperty("line.separator");
		html += "						<div class=\"panel-body\"> " + System.getProperty("line.separator");
		html += "				            <h3 class=\"subject\">" + System.getProperty("line.separator");
		html += "				        	<img src=\"${base}/assets/icon_png/sousuo_small.png\" style=\"margin-right:5px;margin-bottom:5px;\"/>"
				+ System.getProperty("line.separator");
		html += "				                                " + r.getTitle() + System.getProperty("line.separator");
		html += "				                <button type=\"button\" id=\"like\" class=\"btn btn-default pull-right\" data-toggle=\"dropdown\">"
				+ System.getProperty("line.separator");
		html += "				                    <span class=\"star\" id=\"star\">喜欢	</span>"
				+ System.getProperty("line.separator");
		html += "				                </button> " + System.getProperty("line.separator");
		html += "				            </h3>" + System.getProperty("line.separator");
		html += "				            <hr style=\"margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;\"/>"
				+ System.getProperty("line.separator");
		html += "				            <div class=\"content\">" + System.getProperty("line.separator");
		html += "				                <div class=\"message\">" + System.getProperty("line.separator");
		html += "				                    " + r.getContent() + System.getProperty("line.separator");
		html += "				                </div>" + System.getProperty("line.separator");
		html += "				            </div>" + System.getProperty("line.separator");
		html += "				        </div>" + System.getProperty("line.separator");
		html += "				    </div>" + System.getProperty("line.separator");
		html += "				    " + System.getProperty("line.separator");
		html += "		        	<div class=\"panel panel-white border-top-orange\">"
				+ System.getProperty("line.separator");
		html += "		                <div class=\"panel-heading\">" + System.getProperty("line.separator");
		html += "		                    <h3 class=\"panel-title\">解答专区</h3>" + System.getProperty("line.separator");
		html += "		                </div>" + System.getProperty("line.separator");
		html += "		                <div class=\"panel-body\">" + System.getProperty("line.separator");
		html += "		                    <div class=\"panel-group\" id=\"accordion\">"
				+ System.getProperty("line.separator");

		int i = 1;
		for (Question q : qs) {
			html += "		                        <div class=\"panel\">" + System.getProperty("line.separator");
			html += "		                            <div class=\"panel-heading\">"
					+ System.getProperty("line.separator");
			html += "		                                <h4 class=\"panel-title\">"
					+ System.getProperty("line.separator");
			html += "		                                    <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">"
					+ System.getProperty("line.separator");
			html += "		                                        <span class=\"badge bg-light-green squared\" style=\"margin-right:10px;\">#"
					+ i + " 已解答</span>" + System.getProperty("line.separator");
			if (q.getStar() > 0) {
				html += "		                                         " + q.getStar() + "星评价："
						+ answerStar[q.getStar()] + "<span class=\"pull-right\"><img src=\"${base}/assets/icon_png/star"
						+ q.getStar() + ".png\"/></span>" + System.getProperty("line.separator");
			} else {
				html += "		                                         " + answerStar[q.getStar()]
						+ "<span class=\"pull-right\"><img src=\"${base}/assets/icon_png/star" + q.getStar()
						+ ".png\"/></span>" + System.getProperty("line.separator");
			}
			html += "		                                    </a>" + System.getProperty("line.separator");
			html += "		                                </h4>" + System.getProperty("line.separator");
			html += "		                            </div>" + System.getProperty("line.separator");
			html += "		                            <div id=\"collapseOne\" class=\"panel-collapse collapse in\">"
					+ System.getProperty("line.separator");
			html += "		                                <div class=\"panel-body\">"
					+ System.getProperty("line.separator");
			html += "		                                   	 问题：" + q.getTitle()
					+ System.getProperty("line.separator");
			html += "		                               		<button id=\"daan" + q.getId()
					+ "\" class=\"badge bg-light-blue squared\">￥" + answerMoney + " 查看此问题答案，答案有" + q.getChars()
					+ "个字符。</button>" + System.getProperty("line.separator");
			html += "		                                </div>" + System.getProperty("line.separator");
			html += "		                            </div>" + System.getProperty("line.separator");
			html += "		                        </div>" + System.getProperty("line.separator");
			i++;
		}
		
		if(i==1){
			html+="尚无咨询问题"+System.getProperty("line.separator");
		}

		html += "		                   </div>" + System.getProperty("line.separator");
		html += "		               </div>" + System.getProperty("line.separator");
		html += "		               " + System.getProperty("line.separator");
		html += "		           </div>" + System.getProperty("line.separator");
		html += "		           " + System.getProperty("line.separator");
		html += "		   		</div>  " + System.getProperty("line.separator");
		html += "			</div>" + System.getProperty("line.separator");
		html += "			" + System.getProperty("line.separator");
		html += "		</div> " + System.getProperty("line.separator");
		html += "		</div> " + System.getProperty("line.separator");
		html += "		</div>" + System.getProperty("line.separator");
		html += "		</div>" + System.getProperty("line.separator");
		html += "		</aside>" + System.getProperty("line.separator");
		html += "		</div>" + System.getProperty("line.separator");
		html += "      " + System.getProperty("line.separator");
		html += "      <#include \"../user/public/index/modal.ftl\">" + System.getProperty("line.separator");
		html += "  </body>" + System.getProperty("line.separator");
		html += "  <script src=\"${base}/umeditor/third-party/jquery.min.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/umeditor/umeditor.config.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/umeditor/umeditor.min.js\"></script>" + System.getProperty("line.separator");
		html += "  <script src=\"${base}/umeditor/lang/zh-cn/zh-cn.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/assets/js/bootstrap.min.js\"></script>" + System.getProperty("line.separator");
		html += "  <script class=\"include\" type=\"text/javascript\" src=\"${base}/assets/js/jquery.dcjqaccordion.2.7.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/assets/js/jquery.scrollTo.min.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/assets/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/assets/js/common-scripts.js\"></script>"
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/curoAdmin/js/summernote/summernote.min.js\"></script>  "
				+ System.getProperty("line.separator");
		html += "  <script src=\"${base}/curoAdmin/js/message-item-demo.js\"></script> "
				+ System.getProperty("line.separator");
		html += "  <script>" + System.getProperty("line.separator");
		html += "  		var myQuestion = UM.getEditor('myQuestion');" + System.getProperty("line.separator");
		html += "        $(document).ready(function(){" + System.getProperty("line.separator");
		html += "            $(\"#show\").bind(\"click\",function(){" + System.getProperty("line.separator");
		html += "            	var url = window.open();" + System.getProperty("line.separator");
		html += "                var id = $('#r_id').val();" + System.getProperty("line.separator");
		html += "                var allData = {" + System.getProperty("line.separator");
		html += "	　　　　　　　　　　  id:id" + System.getProperty("line.separator");
		html += "	　　　　　　　　  };" + System.getProperty("line.separator");
		html += "		        $.ajax({" + System.getProperty("line.separator");
		html += "	               url:'${base}/resource/show'," + System.getProperty("line.separator");
		html += "	               type:'post'," + System.getProperty("line.separator");
		html += "	               contentType:'application/json;charset=UTF-8',"
				+ System.getProperty("line.separator");
		html += "				   dataType:'json'," + System.getProperty("line.separator");
		html += "	               data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "	               success:function(data){" + System.getProperty("line.separator");
		html += "	               		if(data.status==0){" + System.getProperty("line.separator");
		html += "	               			url.close();" + System.getProperty("line.separator");
		html += "		               		alert(data.message);" + System.getProperty("line.separator");
		html += "	               		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "		               		url.location = data.message;" + System.getProperty("line.separator");
		html += "	               		}" + System.getProperty("line.separator");
		html += "	               }," + System.getProperty("line.separator");
		html += "	               error:function(){" + System.getProperty("line.separator");
		html += "						alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "	               }" + System.getProperty("line.separator");
		html += "		         });" + System.getProperty("line.separator");
		html += "            });" + System.getProperty("line.separator");
		html += "            $(\"#down\").bind(\"click\",function(){" + System.getProperty("line.separator");
		html += "                var id = $('#r_id').val();" + System.getProperty("line.separator");
		html += "                var allData = {" + System.getProperty("line.separator");
		html += "	　　　　　　　　　　  id:id" + System.getProperty("line.separator");
		html += "	　　　　　　　　  };" + System.getProperty("line.separator");
		html += "		        $.ajax({" + System.getProperty("line.separator");
		html += "	               url:'${base}/resource/down'," + System.getProperty("line.separator");
		html += "	               type:'post'," + System.getProperty("line.separator");
		html += "	               contentType:'application/json;charset=UTF-8',"
				+ System.getProperty("line.separator");
		html += "				   dataType:'json'," + System.getProperty("line.separator");
		html += "	               data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "	               success:function(data){" + System.getProperty("line.separator");
		html += "	               		if(data.status==0){" + System.getProperty("line.separator");
		html += "		               		alert(data.message);" + System.getProperty("line.separator");
		html += "	               		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "		               		$(\"#downUrl\").html(data.message);" + System.getProperty("line.separator");
		html += "	               		}" + System.getProperty("line.separator");
		html += "	               }," + System.getProperty("line.separator");
		html += "	               error:function(){" + System.getProperty("line.separator");
		html += "						alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "	               }" + System.getProperty("line.separator");
		html += "		         });" + System.getProperty("line.separator");
		html += "            });" + System.getProperty("line.separator");
		html += "            $(\"#like\").bind(\"click\",function(){" + System.getProperty("line.separator");
		html += "                var id = $('#r_id').val();" + System.getProperty("line.separator");
		html += "                var allData = {" + System.getProperty("line.separator");
		html += "	　　　　　　　　　　  id:id" + System.getProperty("line.separator");
		html += "	　　　　　　　　  };" + System.getProperty("line.separator");
		html += "		        $.ajax({" + System.getProperty("line.separator");
		html += "	               url:'${base}/resource/like'," + System.getProperty("line.separator");
		html += "	               type:'post'," + System.getProperty("line.separator");
		html += "	               contentType:'application/json;charset=UTF-8',"
				+ System.getProperty("line.separator");
		html += "				   dataType:'json'," + System.getProperty("line.separator");
		html += "	               data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "	               success:function(data){" + System.getProperty("line.separator");
		html += "	               		if(data.status==0){" + System.getProperty("line.separator");
		html += "		               		alert(data.message);" + System.getProperty("line.separator");
		html += "		               		return false;" + System.getProperty("line.separator");
		html += "	               		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "		               		$(\"#star\").html(\"<img src='${base}/assets/icon_png/xihuan.png' style='width:15px;height:15px;margin-right:5px;margin-bottom:5px;'/>喜欢\");"
				+ System.getProperty("line.separator");
		html += "	               		}" + System.getProperty("line.separator");
		html += "	               }," + System.getProperty("line.separator");
		html += "	               error:function(){" + System.getProperty("line.separator");
		html += "						alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "	               }" + System.getProperty("line.separator");
		html += "		         });" + System.getProperty("line.separator");
		html += "            });" + System.getProperty("line.separator");
		html += "            $(\"[id^=fuwu]\").bind(\"click\",function(){" + System.getProperty("line.separator");
		html += "            	var id = $('#r_id').val();" + System.getProperty("line.separator");
		html += "		        var way=$(this).attr(\"id\").replace(\"fuwu\",\"\");"
				+ System.getProperty("line.separator");
		html += "		        var qq = $('#qq'+way).val();" + System.getProperty("line.separator");
		html += "            	if(qq==\"\"||typeof(qq)===\"undefined\"){" + System.getProperty("line.separator");
		html += "					alert(\"请输入您的QQ号码！以便作者联系您完成项目的部署。\");" + System.getProperty("line.separator");
		html += "				    $('#qq'+way).focus();" + System.getProperty("line.separator");
		html += "				    return false;" + System.getProperty("line.separator");
		html += "				}" + System.getProperty("line.separator");
		html += "		        var allData = {" + System.getProperty("line.separator");
		html += "		        	  id:id," + System.getProperty("line.separator");
		html += "	　　　　　　　　　　      way:way," + System.getProperty("line.separator");
		html += "					  qq:qq" + System.getProperty("line.separator");
		html += "	　　　　　　　　   };" + System.getProperty("line.separator");
		html += "		        $.ajax({" + System.getProperty("line.separator");
		html += "		           url:'${base}/resource/fuwu'," + System.getProperty("line.separator");
		html += "		           type:'post'," + System.getProperty("line.separator");
		html += "		           contentType:'application/json;charset=UTF-8',"
				+ System.getProperty("line.separator");
		html += "				   dataType:'json'," + System.getProperty("line.separator");
		html += "		           data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "		           beforeSend: function () {" + System.getProperty("line.separator");
		html += "			        // 禁用按钮防止重复提交，发送前响应" + System.getProperty("line.separator");
		html += "			        $(\"#fuwu\"+way).attr({ disabled: \"disabled\" });"
				+ System.getProperty("line.separator");
		html += "	                $(\"#span\"+way).text(\"正在将您的远程部署服务申请通过邮件发送给源码作者。。。\");"
				+ System.getProperty("line.separator");
		html += "			       }," + System.getProperty("line.separator");
		html += "		           success:function(data){" + System.getProperty("line.separator");
		html += "			           	alert(data.message);" + System.getProperty("line.separator");
		html += "			           		if(data.status==0){" + System.getProperty("line.separator");
		html += "		           			$(\"#fuwu\"+way).removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "			                $(\"#span\"+way).text(\"申请\");" + System.getProperty("line.separator");
		html += "		           		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "		                    window.location.reload();" + System.getProperty("line.separator");
		html += "		           		}" + System.getProperty("line.separator");
		html += "		           }," + System.getProperty("line.separator");
		html += "		           error:function(){" + System.getProperty("line.separator");
		html += "						alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "						$(\"#fuwu\"+way).removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "		                $(\"#span\"+way).text(\"申请\");" + System.getProperty("line.separator");
		html += "						" + System.getProperty("line.separator");
		html += "		           }" + System.getProperty("line.separator");
		html += "		         });" + System.getProperty("line.separator");
		html += "	        });" + System.getProperty("line.separator");
		html += "	        $(\"[id^=daan]\").bind(\"click\",function(){" + System.getProperty("line.separator");
		html += "		        var id=$(this).attr(\"id\").replace(\"daan\",\"\");"
				+ System.getProperty("line.separator");
		html += "		        var content=$(\"#daan\"+id).html();" + System.getProperty("line.separator");
		html += "		        var allData = {" + System.getProperty("line.separator");
		html += "	        	   id:id" + System.getProperty("line.separator");
		html += "	　　　　　　　　   };" + System.getProperty("line.separator");
		html += "		        $.ajax({" + System.getProperty("line.separator");
		html += "		           url:'${base}/resource/daan'," + System.getProperty("line.separator");
		html += "		           type:'post'," + System.getProperty("line.separator");
		html += "		           contentType:'application/json;charset=UTF-8',"
				+ System.getProperty("line.separator");
		html += "				   dataType:'json'," + System.getProperty("line.separator");
		html += "		           data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "		           beforeSend: function () {" + System.getProperty("line.separator");
		html += "			        // 禁用按钮防止重复提交，发送前响应" + System.getProperty("line.separator");
		html += "			        $(\"#daan\"+id).attr({ disabled: \"disabled\" });"
				+ System.getProperty("line.separator");
		html += "	                $(\"#daan\"+id).text(\"正在将此答案通过邮件发送到您的注册邮箱。。。\");"
				+ System.getProperty("line.separator");
		html += "			       }," + System.getProperty("line.separator");
		html += "		           success:function(data){" + System.getProperty("line.separator");
		html += "			           	alert(data.message);" + System.getProperty("line.separator");
		html += "			           	if(data.status==0){" + System.getProperty("line.separator");
		html += "		           			$(\"#daan\"+id).removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "			                $(\"#daan\"+id).text(content);" + System.getProperty("line.separator");
		html += "		           		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "		                    window.location.reload();" + System.getProperty("line.separator");
		html += "		           		}" + System.getProperty("line.separator");
		html += "		           }," + System.getProperty("line.separator");
		html += "		           error:function(){" + System.getProperty("line.separator");
		html += "						alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "						$(\"#daan\"+id).removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "		                $(\"#daan\"+id).text(content);" + System.getProperty("line.separator");
		html += "						" + System.getProperty("line.separator");
		html += "		           }" + System.getProperty("line.separator");
		html += "		         });" + System.getProperty("line.separator");
		html += "	        });" + System.getProperty("line.separator");
		html += "		});" + System.getProperty("line.separator");
		html += "		function sendQuestion() {" + System.getProperty("line.separator");
		html += "	        var myarr = [];" + System.getProperty("line.separator");
		html += "	        myarr.push(UM.getEditor('myQuestion').getContent());"
				+ System.getProperty("line.separator");
		html += "	        if(myarr==\"\"){" + System.getProperty("line.separator");
		html += "	          alert(\"请详细描述问题！\");" + System.getProperty("line.separator");
		html += "	          UM.getEditor('myQuestion').focus();" + System.getProperty("line.separator");
		html += "			  return false;" + System.getProperty("line.separator");
		html += "	        }" + System.getProperty("line.separator");
		html += "	        var id = $('#r_id').val();" + System.getProperty("line.separator");
		html += "	        var title = myarr.join(\"\");" + System.getProperty("line.separator");
		html += "	        var allData = {" + System.getProperty("line.separator");
		html += "	　　　　　　　　 id:id," + System.getProperty("line.separator");
		html += "			   title:title" + System.getProperty("line.separator");
		html += "	　　　　　　};" + System.getProperty("line.separator");
		html += "	        $.ajax({" + System.getProperty("line.separator");
		html += "	           url:'${base}/resource/question'," + System.getProperty("line.separator");
		html += "	           type:'post'," + System.getProperty("line.separator");
		html += "	           contentType:'application/json;charset=UTF-8'," + System.getProperty("line.separator");
		html += "			   dataType:'json'," + System.getProperty("line.separator");
		html += "	           data:JSON.stringify(allData)," + System.getProperty("line.separator");
		html += "	           beforeSend: function () {" + System.getProperty("line.separator");
		html += "			        // 禁用按钮防止重复提交，发送前响应" + System.getProperty("line.separator");
		html += "			        $(\"#sendQuestionButton\").attr({ disabled: \"disabled\" });"
				+ System.getProperty("line.separator");
		html += "			        $(\"#sendQuestionCancel\").attr({ disabled: \"disabled\" });"
				+ System.getProperty("line.separator");
		html += "	                $('#sendQuestionButton').text(\"正在将您的问题通过邮件发送给源码作者。。。\");"
				+ System.getProperty("line.separator");
		html += "			    }," + System.getProperty("line.separator");
		html += "	           success:function(data){" + System.getProperty("line.separator");
		html += "	           		alert(data.message);" + System.getProperty("line.separator");
		html += "	           		if(data.status==0){" + System.getProperty("line.separator");
		html += "	           			$('#sendQuestionButton').removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "	           			$('#sendQuestionCancel').removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "	           			$('#sendQuestionButton').text(\"￥" + zixunMoney + " 咨询\");"
				+ System.getProperty("line.separator");
		html += "	           		}else if(data.status==1){" + System.getProperty("line.separator");
		html += "	                    window.location.reload();" + System.getProperty("line.separator");
		html += "	           		}" + System.getProperty("line.separator");
		html += "	           }," + System.getProperty("line.separator");
		html += "	           error:function(){" + System.getProperty("line.separator");
		html += "					alert(\"服务器错误！请联系站长\");" + System.getProperty("line.separator");
		html += "					$('#sendQuestionButton').removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "					$('#sendQuestionCancel').removeAttr(\"disabled\");"
				+ System.getProperty("line.separator");
		html += "	           }" + System.getProperty("line.separator");
		html += "	         });" + System.getProperty("line.separator");
		html += "      	}" + System.getProperty("line.separator");
		html += "    </script>" + System.getProperty("line.separator");
		html += "</html>" + System.getProperty("line.separator");
		return html;
	}

	public static String allDemandListToHtml(List<Demands> allDemands, int state) {
		String html = "";
		int i=0;int j=1;
		for(Demands ds : allDemands){
			html+="<tr>" + System.getProperty("line.separator");
			html+="	<td>"+j+"</td>" + System.getProperty("line.separator");
			html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\"><img src=\"${base}/user/"+ds.getPhoto()+"\" style=\"height: 45px;width: 45px;border-radius: 50%;margin: 0 10px;vertical-align: middle;\">" + System.getProperty("line.separator");
			html+="	<span class=\"name\">"+ds.getName()+"</span></a></td>" + System.getProperty("line.separator");
			if("1".equals(ds.getState())){
				html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\"><span class=\"label label-light-blue\">待解决</span></a></td>" + System.getProperty("line.separator");
			}else if("2".equals(ds.getState())){
				html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\"><span class=\"label label-light-purple\">待验证</span></a></td>" + System.getProperty("line.separator");
			}else if("3".equals(ds.getState())){
				html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\"><span class=\"label label-light-green\">已解决</span></a></td>" + System.getProperty("line.separator");
			}else{
				html+="    <td></td>" + System.getProperty("line.separator");
			}
			html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\"><span class=\"subject\">"+ds.getTitle()+"</span></a></td>" + System.getProperty("line.separator");
			html+="    <td><a href=\"${base}/demand/"+ds.getId()+".html\">"+TimeTools.getTimeFormatText(ds.getTime())+"</a></td>" + System.getProperty("line.separator");
			html+="</tr>" + System.getProperty("line.separator");
			i++;j++;
		}
		if(state==1){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "mainNumber.ftl", false,String.valueOf(i));
		}else if(state==2){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "waitNumber.ftl", false,String.valueOf(i));
		}else if(state==3){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "validateNumber.ftl", false,String.valueOf(i));
		}else if(state==4){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "completeNumber.ftl", false,String.valueOf(i));
		}else if(state==5){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "vipNumber.ftl", false,String.valueOf(i));
		}else if(state==6){
			TextFileTools.write(ScheduledTasks.getDemandMainFilePath() + "supportNumber.ftl", false,String.valueOf(i));
		}
		return html;
	}

	public static String demandDetailToHtml(Demand demand, List<Answer> answers) {
		String html="";
		html+="<#assign base=request.contextPath />"+System.getProperty("line.separator");
		html+="<!DOCTYPE html>"+System.getProperty("line.separator");
		html+="<html>"+System.getProperty("line.separator");
		html+="  <head>"+System.getProperty("line.separator");
		html+="    <meta charset=\"utf-8\">"+System.getProperty("line.separator");
		html+="    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"+System.getProperty("line.separator");
		html+="    <meta name=\"description\" content=\"发现源码 - 发现您需要的源码，在线演示和免费下载。\">"+System.getProperty("line.separator");
		html+="    <meta name=\"author\" content=\"蜗牛库\">"+System.getProperty("line.separator");
		html+="    <meta name=\"keyword\" content=\"搜索源码, 免费下载, 在线演示, 技术支持, 咨询服务\">"+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="    <title>"+demand.getTitle()+"</title>"+System.getProperty("line.separator");
		html+="    <link href=\"${base}/assets/img/logo.ico\" rel=\"shortcut icon\">"+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="     <!-- Bootstrap core CSS -->"+System.getProperty("line.separator");
		html+="    <link href=\"${base}/assets/css/bootstrap.css\" rel=\"stylesheet\">"+System.getProperty("line.separator");
		html+="    <!--external css-->"+System.getProperty("line.separator");
		html+="    <link href=\"${base}/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />"+System.getProperty("line.separator");
		html+="    "+System.getProperty("line.separator");
		html+="    <!-- Custom styles for this template -->"+System.getProperty("line.separator");
		html+="    <link href=\"${base}/assets/css/style.css\" rel=\"stylesheet\">"+System.getProperty("line.separator");
		html+="    <link href=\"${base}/assets/css/style-responsive.css\" rel=\"stylesheet\">"+System.getProperty("line.separator");
		html+="    <link rel=\"stylesheet\" href=\"${base}/assets/css/to-do.css\">"+System.getProperty("line.separator");
		html+="    "+System.getProperty("line.separator");
		html+="    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/summernote/summernote.css\">"+System.getProperty("line.separator");
		html+="    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/summernote/summernote-bs3.css\">"+System.getProperty("line.separator");
		html+="    "+System.getProperty("line.separator");
		html+="    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/curoAdmin/css/style.css\" /> "+System.getProperty("line.separator");
		html+="    "+System.getProperty("line.separator");
		html+="    <link rel=\"stylesheet\" type=\"text/css\" href=\"${base}/umeditor/themes/default/css/umeditor.css\" /> "+System.getProperty("line.separator");
		html+="    "+System.getProperty("line.separator");
		html+="    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->"+System.getProperty("line.separator");
		html+="    <!--[if lt IE 9]>"+System.getProperty("line.separator");
		html+="      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>"+System.getProperty("line.separator");
		html+="      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>"+System.getProperty("line.separator");
		html+="    <![endif]-->"+System.getProperty("line.separator");
		html+="  </head>"+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="  <body>"+System.getProperty("line.separator");
		html+="  	  <input type=\"hidden\" id=\"d_id\" value=\""+demand.getId()+"\">"+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="      <#include \"../user/public/index/top.ftl\">"+System.getProperty("line.separator");
		html+="      <#include \"../user/public/index/left.ftl\">"+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="	  <div class=\"header-fixed skin-blue\">"+System.getProperty("line.separator");
		html+="		<aside class=\"content-wrapper collapse sidebarLeft\">"+System.getProperty("line.separator");
		html+="		<div class=\"content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper\"> "+System.getProperty("line.separator");
		html+="		<div class=\"panel panel-white\">"+System.getProperty("line.separator");
		html+="		<div class=\"panel-body\"> "+System.getProperty("line.separator");
		html+="		<div class=\"row\">"+System.getProperty("line.separator");
		html+="			"+System.getProperty("line.separator");
		html+="			<#include \"../user/public/demand/left.ftl\">"+System.getProperty("line.separator");
		html+="			 "+System.getProperty("line.separator");
		html+="		    <div class=\"col-sm-9 col-md-10\" id=\"demandContent\">"+System.getProperty("line.separator");
		html+="		        <div class=\"attatchments-wrapper\">"+System.getProperty("line.separator");
		html+="		    	  <div class=\"panel panel-white\">"+System.getProperty("line.separator");
		html+="					<div class=\"panel-body\"> "+System.getProperty("line.separator");
		html+="					<h3 class=\"subject\">"+System.getProperty("line.separator");
		html+="		        	<img src=\"${base}/assets/icon_png/xuqiu_small.png\" style=\"margin-right:5px;margin-bottom:5px;\"/>"+System.getProperty("line.separator");
		html+="		                                "+demand.getTitle()+System.getProperty("line.separator");
		html+="		                <button type=\"button\" class=\"btn btn-default pull-right\" data-toggle=\"dropdown\">"+System.getProperty("line.separator");
		html+="		                    <span class=\"star\" id=\"guanzhu\">关注</span>"+System.getProperty("line.separator");
		html+="		                </button> "+System.getProperty("line.separator");
		html+="		            </h3>"+System.getProperty("line.separator");
		html+="		            <hr style=\"margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;\"/>"+System.getProperty("line.separator");
		html+="		            "+System.getProperty("line.separator");
		html+="		            <div class=\"content\">"+System.getProperty("line.separator");
		html+="		                <div class=\"message\">"+System.getProperty("line.separator");
		html+="		                    "+demand.getContent()+System.getProperty("line.separator");
		html+="		                </div>"+System.getProperty("line.separator");
		html+="		        	</div> "+System.getProperty("line.separator");
		html+="		        	"+System.getProperty("line.separator");
		html+="		        	</div>"+System.getProperty("line.separator");
		html+="		          </div>"+System.getProperty("line.separator");
		html+="		        </div>"+System.getProperty("line.separator");
		html+="		        "+System.getProperty("line.separator");
		html+="		        <div class=\"attatchments-wrapper\">"+System.getProperty("line.separator");
		html+="		                    	"+System.getProperty("line.separator");
		html+="		        	<div class=\"panel panel-white border-top-orange\">"+System.getProperty("line.separator");
		html+="		                <div class=\"panel-heading\">"+System.getProperty("line.separator");
		html+="		                    <h3 class=\"panel-title\">解决方案</h3>"+System.getProperty("line.separator");
		html+="		                </div>"+System.getProperty("line.separator");
		html+="		                <div class=\"panel-body\">"+System.getProperty("line.separator");
		html+="		                    <div class=\"panel-group\" id=\"accordion\">"+System.getProperty("line.separator");
		html+="		                        "+System.getProperty("line.separator");
		int i=1;
		for(Answer answer : answers){
			html+="		                        <div class=\"panel\">"+System.getProperty("line.separator");
			html+="		                            <div class=\"panel-heading\">"+System.getProperty("line.separator");
			html+="		                                <h4 class=\"panel-title\">"+System.getProperty("line.separator");
			html+="		                                    <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\">"+System.getProperty("line.separator");
			if(answer.getState()==1){
				html+="		                                        <span class=\"badge bg-light-blue squared\" style=\"margin-right:10px;\">待验证</span>#"+i+System.getProperty("line.separator");
			}else if(answer.getState()==2){
				html+="		                                        <span class=\"badge bg-light-orange squared\" style=\"margin-right:10px;\">未解决</span>#"+i+System.getProperty("line.separator");
			}else if(answer.getState()==3){
				html+="		                                        <span class=\"badge bg-light-green squared\" style=\"margin-right:10px;\">已解决</span>#"+i+System.getProperty("line.separator");
			}
			
			html+="		                                    </a>"+System.getProperty("line.separator");
			html+="		                                </h4>"+System.getProperty("line.separator");
			html+="		                            </div>"+System.getProperty("line.separator");
			html+="		                            <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">"+System.getProperty("line.separator");
			html+="		                                <div class=\"panel-body\">"+System.getProperty("line.separator");
			html+="		                                    "+answer.getContent()+System.getProperty("line.separator");
			html+="		                                </div>"+System.getProperty("line.separator");
			html+="		                            </div>"+System.getProperty("line.separator");
			html+="		                        </div>"+System.getProperty("line.separator");
			i++;
		}
		if(i==1){
			html+="尚无解决方案"+System.getProperty("line.separator");
		}
		
		html+="		                        "+System.getProperty("line.separator");
		html+="		                    </div>"+System.getProperty("line.separator");
		html+="		                </div>"+System.getProperty("line.separator");
		html+="		            </div>"+System.getProperty("line.separator");
		html+="		   	    </div>"+System.getProperty("line.separator");
		html+="		   	    "+System.getProperty("line.separator");
		html+="		   	    <#if user?exists>"+System.getProperty("line.separator");
		html+="			    <div class=\"reply\" style=\"margin-top:20px;\">"+System.getProperty("line.separator");
		html+="			    	<script type=\"text/plain\" id=\"myAnswer\"></script>"+System.getProperty("line.separator");
		html+="			    	<br/>"+System.getProperty("line.separator");
		html+="			        <a onclick=\"sendAnswer()\" id=\"sendAnswerButton\" class=\"btn btn-green reply-btn\">发布新的解决方案</a>"+System.getProperty("line.separator");
		html+="			    </div>"+System.getProperty("line.separator");
		html+="		        </#if>"+System.getProperty("line.separator");
		html+="		        "+System.getProperty("line.separator");
		html+="		    </div>"+System.getProperty("line.separator");
		html+="			"+System.getProperty("line.separator");
		html+="		</div> "+System.getProperty("line.separator");
		html+="		</div> "+System.getProperty("line.separator");
		html+="		</div>"+System.getProperty("line.separator");
		html+="		</div>"+System.getProperty("line.separator");
		html+="		</aside>"+System.getProperty("line.separator");
		html+="		</div>      "+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="      <#include \"../user/public/index/modal.ftl\">"+System.getProperty("line.separator");
		html+="  </body>"+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="  <!-- js placed at the end of the document so the pages load faster -->"+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="  <script src=\"${base}/umeditor/third-party/jquery.min.js\"></script>"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/umeditor/umeditor.config.js\"></script>"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/umeditor/umeditor.min.js\"></script>"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/umeditor/lang/zh-cn/zh-cn.js\"></script>"+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="  <script src=\"${base}/assets/js/bootstrap.min.js\"></script>"+System.getProperty("line.separator");
		html+="  <script class=\"include\" type=\"text/javascript\" src=\"${base}/assets/js/jquery.dcjqaccordion.2.7.js\"></script>"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/assets/js/jquery.scrollTo.min.js\"></script>"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/assets/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>"+System.getProperty("line.separator");
		html+=""+System.getProperty("line.separator");
		html+="  <!--common script for all pages-->"+System.getProperty("line.separator");
		html+="  <script src=\"${base}/assets/js/common-scripts.js\"></script>"+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="  <script src=\"${base}/curoAdmin/js/summernote/summernote.min.js\"></script>  "+System.getProperty("line.separator");
		html+="  <script src=\"${base}/curoAdmin/js/message-item-demo.js\"></script> "+System.getProperty("line.separator");
		html+="  "+System.getProperty("line.separator");
		html+="  <script type=\"text/javascript\">"+System.getProperty("line.separator");
		html+="	  //实例化编辑器"+System.getProperty("line.separator");
		html+="      var um = UM.getEditor('myDemand');"+System.getProperty("line.separator");
		html+="      var myanswer = UM.getEditor('myAnswer');"+System.getProperty("line.separator");
		html+="      function sendDemand() {"+System.getProperty("line.separator");
		html+="        var title=$(\"#title\").val();"+System.getProperty("line.separator");
		html+="	  	if(title==\"\"){"+System.getProperty("line.separator");
		html+="		  alert(\"请输入标题！\");"+System.getProperty("line.separator");
		html+="		  $('#title').focus();"+System.getProperty("line.separator");
		html+="		  return false;"+System.getProperty("line.separator");
		html+="		}"+System.getProperty("line.separator");
		html+="        var arr = [];"+System.getProperty("line.separator");
		html+="        arr.push(UM.getEditor('myDemand').getContent());"+System.getProperty("line.separator");
		html+="        if(arr==\"\"){"+System.getProperty("line.separator");
		html+="          alert(\"请输入内容！\");"+System.getProperty("line.separator");
		html+="          UM.getEditor('myDemand').focus();"+System.getProperty("line.separator");
		html+="		  return false;"+System.getProperty("line.separator");
		html+="        }"+System.getProperty("line.separator");
		html+="        var content = arr.join(\"\");"+System.getProperty("line.separator");
		html+="        var allData = {"+System.getProperty("line.separator");
		html+="　　　　　　　    title:title,"+System.getProperty("line.separator");
		html+="           content:content"+System.getProperty("line.separator");
		html+="　　　　　 };"+System.getProperty("line.separator");
		html+="        $.ajax({"+System.getProperty("line.separator");
		html+="           url:'${base}/user/sendDemand',"+System.getProperty("line.separator");
		html+="           type:'post',"+System.getProperty("line.separator");
		html+="           contentType:'application/json;charset=UTF-8',"+System.getProperty("line.separator");
		html+="		   dataType:'json',"+System.getProperty("line.separator");
		html+="           data:JSON.stringify(allData),"+System.getProperty("line.separator");
		html+="           success:function(data){"+System.getProperty("line.separator");
		html+="           		if(data.status==1){"+System.getProperty("line.separator");
		html+="           			alert(\"发布需求成功！您的需求将会在5分钟内加入到需求列表中，请耐心等待。\");"+System.getProperty("line.separator");
		html+="                    window.location.href = \"${base}/demand.html\";"+System.getProperty("line.separator");
		html+="           		}else{"+System.getProperty("line.separator");
		html+="	           		alert(data.message);"+System.getProperty("line.separator");
		html+="           		}"+System.getProperty("line.separator");
		html+="           },"+System.getProperty("line.separator");
		html+="           error:function(){"+System.getProperty("line.separator");
		html+="				alert(\"服务器错误！请联系站长\");"+System.getProperty("line.separator");
		html+="           }"+System.getProperty("line.separator");
		html+="        })"+System.getProperty("line.separator");
		html+="      }"+System.getProperty("line.separator");
		html+="      function sendAnswer() {"+System.getProperty("line.separator");
		html+="        var myarr = [];"+System.getProperty("line.separator");
		html+="        myarr.push(UM.getEditor('myAnswer').getContent());"+System.getProperty("line.separator");
		html+="        if(myarr==\"\"){"+System.getProperty("line.separator");
		html+="          alert(\"请输入内容！\");"+System.getProperty("line.separator");
		html+="          UM.getEditor('myAnswer').focus();"+System.getProperty("line.separator");
		html+="		  return false;"+System.getProperty("line.separator");
		html+="        }"+System.getProperty("line.separator");
		html+="        var id = $('#d_id').val();"+System.getProperty("line.separator");
		html+="        var content = myarr.join(\"\");"+System.getProperty("line.separator");
		html+="        var allData = {"+System.getProperty("line.separator");
		html+="　　　　　　　  id:id,"+System.getProperty("line.separator");
		html+="		  content:content"+System.getProperty("line.separator");
		html+="　　　　　　};"+System.getProperty("line.separator");
		html+="        $.ajax({"+System.getProperty("line.separator");
		html+="           url:'${base}/demand/answer',"+System.getProperty("line.separator");
		html+="           type:'post',"+System.getProperty("line.separator");
		html+="           contentType:'application/json;charset=UTF-8',"+System.getProperty("line.separator");
		html+="		   dataType:'json',"+System.getProperty("line.separator");
		html+="           data:JSON.stringify(allData),"+System.getProperty("line.separator");
		html+="           beforeSend: function () {"+System.getProperty("line.separator");
		html+="		        // 禁用按钮防止重复提交，发送前响应"+System.getProperty("line.separator");
		html+="		        $(\"#sendAnswerButton\").attr({ disabled: \"disabled\" });"+System.getProperty("line.separator");
		html+="                $('#sendAnswerButton').text(\"正在将您的解决方案通过邮件发送给需求用户。。。\");"+System.getProperty("line.separator");
		html+="		    },"+System.getProperty("line.separator");
		html+="           success:function(data){"+System.getProperty("line.separator");
		html+="           		alert(data.message);"+System.getProperty("line.separator");
		html+="           		if(data.status==0){"+System.getProperty("line.separator");
		html+="               		$('#sendAnswerButton').removeAttr(\"disabled\");"+System.getProperty("line.separator");
		html+="           			$('#sendAnswerButton').text(\"发布新的解决方案\");"+System.getProperty("line.separator");
		html+="           		}else if(data.status==1){"+System.getProperty("line.separator");
		html+="               		window.location.href = \"${base}/demand/\"+id+\"_new.html\";"+System.getProperty("line.separator");
		html+="           		}"+System.getProperty("line.separator");
		html+="           },"+System.getProperty("line.separator");
		html+="           error:function(){"+System.getProperty("line.separator");
		html+="				alert(\"服务器错误！请联系站长\");"+System.getProperty("line.separator");
		html+="				$('#sendAnswerButton').removeAttr(\"disabled\");"+System.getProperty("line.separator");
		html+="           		$('#sendAnswerButton').text(\"发布新的解决方案\");"+System.getProperty("line.separator");
		html+="           }"+System.getProperty("line.separator");
		html+="         });"+System.getProperty("line.separator");
		html+="      }"+System.getProperty("line.separator");
		html+="      $(\"#guanzhu\").bind(\"click\",function(){"+System.getProperty("line.separator");
		html+="            var id = $('#d_id').val();"+System.getProperty("line.separator");
		html+="            var allData = {"+System.getProperty("line.separator");
		html+="　　　　　　　　　　  id:id"+System.getProperty("line.separator");
		html+="　　　　　　　　  };"+System.getProperty("line.separator");
		html+="	        $.ajax({"+System.getProperty("line.separator");
		html+="               url:'${base}/demand/guanzhu',"+System.getProperty("line.separator");
		html+="               type:'post',"+System.getProperty("line.separator");
		html+="               contentType:'application/json;charset=UTF-8',"+System.getProperty("line.separator");
		html+="			   dataType:'json',"+System.getProperty("line.separator");
		html+="               data:JSON.stringify(allData),"+System.getProperty("line.separator");
		html+="               success:function(data){"+System.getProperty("line.separator");
		html+="               		if(data.status==0){"+System.getProperty("line.separator");
		html+="	               		alert(data.message);"+System.getProperty("line.separator");
		html+="	               		return false;"+System.getProperty("line.separator");
		html+="               		}else if(data.status==1){"+System.getProperty("line.separator");
		html+="	               		$(\"#guanzhu\").html(\"<i class='fa fa-star color-light-orange'></i>已关注\");"+System.getProperty("line.separator");
		html+="               		}"+System.getProperty("line.separator");
		html+="               },"+System.getProperty("line.separator");
		html+="               error:function(){"+System.getProperty("line.separator");
		html+="					alert(\"服务器错误！请联系站长\");"+System.getProperty("line.separator");
		html+="               }"+System.getProperty("line.separator");
		html+="	         });"+System.getProperty("line.separator");
		html+="        });"+System.getProperty("line.separator");
		html+="  </script> "+System.getProperty("line.separator");
		html+="</html>"+System.getProperty("line.separator");
		return html;
	}

	public static String indexYanShi(List<FocusR> frs) {
		String html="";
		html+="<#assign base=request.contextPath />"+System.getProperty("line.separator");
		html+="<h3>在线演示</h3>"+System.getProperty("line.separator");
		for(FocusR fr:frs){
			html+="<div class='desc'>"+System.getProperty("line.separator");
			html+="<div class='thumb'><span class='badge bg-theme'><i class='fa fa-clock-o'></i></span></div>"+System.getProperty("line.separator");
			html+="<div style='margin-left:60px;'><p><muted>刚刚</muted></p>"+System.getProperty("line.separator");
			html+=fr.getName()+"在线演示：<br/><a href='${base}/resource/"+fr.getId()+".html'>"+fr.getTitle()+"</a></div>"+System.getProperty("line.separator");
			html+="</div>"+System.getProperty("line.separator");
		}
		return html;
	}

	public static String indexXiaZai(List<FocusR> frs) {
		String html="";
		html+="<#assign base=request.contextPath />"+System.getProperty("line.separator");
		html+="<h3>免费下载</h3>"+System.getProperty("line.separator");
		for(FocusR fr:frs){
			html+="<div class='desc'>"+System.getProperty("line.separator");
			html+="<div class='thumb'><img class='img-circle' src='${base}/user/"+fr.getPhoto()+"' width='35px' height='35px' align=''></div>"+System.getProperty("line.separator");
			html+="<div style='margin-left:63px;'>"+System.getProperty("line.separator");
			html+="<i>"+fr.getName()+"免费下载</i><br/><a href='${base}/resource/"+fr.getId()+".html'>"+fr.getTitle()+"</a>"+System.getProperty("line.separator");
			html+="</div></div>"+System.getProperty("line.separator");
		}
		return html;
	}

	public static String indexBiaoQian(List<BiaoQian> bqs) {
		String html="";
		html+="<#assign base=request.contextPath />"+System.getProperty("line.separator");
		html+="<h3>源码标签</h3>"+System.getProperty("line.separator");
		html+="<div id='tagscloud'>"+System.getProperty("line.separator");
		int i=0;
		for(BiaoQian bq:bqs){
			html+="<a href='${base}/resource/"+bq.getId()+".html' class='"+resourceClass[i]+"'>"+bq.getTitle()+"</a>"+System.getProperty("line.separator");
			i++;
			if (i > 2) {
				i = 0;
			}
		}
		html+="</div>"+System.getProperty("line.separator");
		return html;
	}

	public static String indexYuanMa(List<BiaoQian> bqs) {
		String html="";
		html+="<#assign base=request.contextPath />"+System.getProperty("line.separator");
		html+="<div class='row'>"+System.getProperty("line.separator");
		html+="<div class='border-head'><h3>最新源码<span style=\"margin-left:10px;color:red;font-size:12px;\">*建议使用谷歌或火狐浏览器在线演示</span></h3></div>"+System.getProperty("line.separator");
		for(BiaoQian bq:bqs){
			html+="<div class='col-md-4 col-sm-4 mb'>"+System.getProperty("line.separator");
			html+="<div class='green-panel pn'>"+System.getProperty("line.separator");
			html+="<div class='green-header'><a href='${base}/resource/"+bq.getId()+".html'><h5>"+bq.getTitle()+"</h5></a></div>"+System.getProperty("line.separator");
			html+="<div class='chart'><div id='profile-02' style='background: url("+bq.getBackground()+") no-repeat center top;background-size: 100% 100%;  -moz-background-size: 100% 100%;-webkit-background-size: 100% 100%;  '></div></div>"+System.getProperty("line.separator");
			html+="</div>"+System.getProperty("line.separator");
			html+="</div>"+System.getProperty("line.separator");
		}
		html+="</div>"+System.getProperty("line.separator");
		return html;
	}

	public static String allCostList(List<CostList> costListByWay3) {
		String html="";
		int i=1;
		for(CostList cl:costListByWay3){
			html+="<tr>"+System.getProperty("line.separator");
			html+="<td>"+i+"</td>"+System.getProperty("line.separator");
			html+="<td><img src='${base}/user/"+cl.getPhoto()+"' style='height: 45px;width: 45px;border-radius: 50%;margin: 0 10px;vertical-align: middle;'>"+System.getProperty("line.separator");
			html+="<span class='name'>"+cl.getName()+"</span></td>"+System.getProperty("line.separator");
			html+="<td>赞助</td>"+System.getProperty("line.separator");
			html+="<td>"+cl.getAmount()+"</td>"+System.getProperty("line.separator");
			html+="<td>"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cl.getTime())+"</td>"+System.getProperty("line.separator");
			html+="</tr>"+System.getProperty("line.separator");
			i++;
		}
		return html;
	}

	public static String allUsers(List<User> users) {
		String html="";
		int i=1;
		for(User u:users){
			html+="<tr>"+System.getProperty("line.separator");
			html+="<td>"+i+"</td>"+System.getProperty("line.separator");
			html+="<td><span class='name'>"+u.getName()+"</span></td>"+System.getProperty("line.separator");
			html+="<td>"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(u.getTime())+"</td>"+System.getProperty("line.separator");
			html+="</tr>"+System.getProperty("line.separator");
			i++;
		}
		return html;
	}

	public static String allVipUsers(List<User> vips) {
		String html="";
		int i=1;
		for(User u:vips){
			html+="<tr>"+System.getProperty("line.separator");
			html+="<td>"+i+"</td>"+System.getProperty("line.separator");
			html+="<td><span class='name'>"+u.getName()+"</span></td>"+System.getProperty("line.separator");
			html+="<td>"+u.getId().substring(0, u.getId().length()-6)+"******"+"</td>"+System.getProperty("line.separator");
			html+="<td>"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(u.getTime())+"</td>"+System.getProperty("line.separator");
			html+="</tr>"+System.getProperty("line.separator");
			i++;
		}
		return html;
	}
	
}
