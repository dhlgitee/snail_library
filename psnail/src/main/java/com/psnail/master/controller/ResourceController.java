package com.psnail.master.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psnail.master.service.ResourceService;
import com.psnail.tools.GsonTools;
import com.psnail.tools.PathTools;
import com.psnail.tools.TextFileTools;

@Controller
@RequestMapping("/resource")
public class ResourceController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ResourceService resourceService;

	/**
	 * 文件不存在，自动生成资源详情页
	 */
	@RequestMapping(value = "/{id}.html", method = RequestMethod.GET)
	public String html(@PathVariable("id") String id) {
		String filePath = PathTools.getPath() + File.separator + "templates"
				+ File.separator + "resource" + File.separator + id + ".ftl";
		File file = new File(filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
				TextFileTools.write(filePath, false,
						resourceService.detail2Html(id));
			} catch (IOException e) {
				logger.info("源码详情界面文件创建失败，路径：" + filePath);
			}
		}
		return "resource/" + id;
	}

	/**
	 * 文件存在，更新资源详情页
	 */
	@RequestMapping(value = "/{id}_new.html", method = RequestMethod.GET)
	public String html_new(@PathVariable("id") String id) {
		String filePath = PathTools.getPath() + File.separator + "templates"
				+ File.separator + "resource" + File.separator + id + ".ftl";
		File file = new File(filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
				TextFileTools.write(filePath, false,
						resourceService.detail2Html(id));
			} catch (IOException e) {
				logger.info("源码详情界面文件创建失败，路径：" + filePath);
			}
		} else {
			TextFileTools.write(filePath, false,
					resourceService.detail2Html(id));
		}
		return "resource/" + id;
	}

	/**
	 * 演示地址
	 */
	@RequestMapping(value = "/show", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String show(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/show()接收的参数id：" + id);
		Map<String, Object> result = resourceService.show(id);
		logger.info("resource/show()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 下载源码
	 */
	@RequestMapping(value = "/down", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String down(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/down()接收的参数id：" + id);
		Map<String, Object> result = resourceService.down(id);
		logger.info("resource/down()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 喜欢源码
	 */
	@RequestMapping(value = "/like", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String like(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/like()接收的参数id：" + id);
		Map<String, Object> result = resourceService.like(id);
		logger.info("resource/like()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 咨询问题
	 */
	@RequestMapping(value = "/question", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String question(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String title = reqMap.get("title").toString();
		logger.info("resource/question()接收的参数id：" + id + "，title：" + title);
		Map<String, Object> result = resourceService.question(id, title);
		logger.info("resource/question()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 部署服务
	 */
	@RequestMapping(value = "/fuwu", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String fuwu(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String way = reqMap.get("way").toString();
		String qq = reqMap.get("qq").toString();
		logger.info("resource/fuwu()接收的参数id：" + id + "，way：" + way + "，qq："
				+ qq);
		Map<String, Object> result = resourceService.fuwu(id,
				Integer.valueOf(way), qq);
		logger.info("resource/fuwu()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 1元查看答案
	 */
	@RequestMapping(value = "/daan", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String daan(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/daan()接收的参数id：" + id);
		// 给用户发送答案
		Map<String, Object> result = resourceService.daan(id);
		// 给作者提成
		if ("1".equals(result.get("status").toString())) {
			resourceService.daanTicheng(id);
		}
		logger.info("resource/daan()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 根据问题id查询资源提问问题
	 */
	@RequestMapping(value = "/getQuestionById", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getQuestionById(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/getQuestionById()接收的参数id：" + id);
		Map<String, Object> result = resourceService.getQuestionById(id);
		logger.info("resource/getQuestionById()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 回答问题答案
	 */
	@RequestMapping(value = "/answer", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String answer(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String content = reqMap.get("content").toString();
		logger.info("resource/answer()接收的参数id：" + id + "，content：" + content);
		Map<String, Object> result = resourceService.answer(id, content);
		logger.info("resource/answer()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 评分
	 */
	@RequestMapping(value = "/star", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String star(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String score = reqMap.get("score").toString();
		logger.info("resource/star()接收的参数id：" + id + "，score：" + score);
		Map<String, Object> result = resourceService.star(id, score);
		// 服务提成
		if ("1".equals(result.get("status").toString())) {
			resourceService.ticheng(id, (BigDecimal) result.get("choulao"));
		}
		logger.info("resource/star()返回结果集：" + result);
		return GsonTools.toJson(result);
	}
}