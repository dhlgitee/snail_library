package com.psnail.master.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psnail.master.service.EmailService;
import com.psnail.tools.GsonTools;

@Controller
@RequestMapping("/email")
public class EmailController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmailService emailService; // 主数据源

	/**
	 * 发送注册邮件
	 */
	@RequestMapping(value = "/zhuce", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String zhuce(@RequestBody String email) {
		logger.info("EmailController/zhuce()接收的邮箱参数：" + email);
		Map<String, Object> result = emailService.zhuce(email);
		logger.info("EmailController/zhuce()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 发送注册邮件
	 */
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String updatePassword(@RequestBody String email) {
		logger.info("EmailController/updatePassword()接收的邮箱参数：" + email);
		Map<String, Object> result = emailService.updatePassword(email);
		logger.info("EmailController/updatePassword()返回结果集：" + result);
		return GsonTools.toJson(result);
	}
}
