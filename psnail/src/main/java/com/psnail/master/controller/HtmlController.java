package com.psnail.master.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.psnail.master.entity.User;

@Controller
public class HtmlController {
	@Autowired
	HttpServletRequest request;
	// 站长邮箱
	@Value("${email.code.zhanzhang}")
	private String zhanzhang;
	// 管理员邮箱
	@Value("${email.code.guanliyuan}")
	private String guanliyuan;

	/**
	 * 用户界面的跳转
	 */
	@RequestMapping(value = "/{address}.html", method = RequestMethod.GET)
	public String html(@PathVariable("address") String address) {
		if ("audit".equals(address)) {
			HttpSession session = request.getSession(true);
			User user = (User) session.getAttribute("user");
			if (user == null) {
				return "user/index";
			} else if (!zhanzhang.contains(user.getEmail()) && !guanliyuan.contains(user.getEmail())) {
				return "user/index";
			} else {
				return "user/" + address;
			}
		}
		return "user/" + address;
	}

}