package com.psnail.master.mapper.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.psnail.master.entity.dto.AllResources;
import com.psnail.master.entity.dto.BiaoQian;
import com.psnail.master.entity.dto.OtherResources;

public interface ResourceDao {

	int countByAll();

	List<AllResources> getAllResources();

	List<OtherResources> getOtherResources(@Param("way") int way);

	List<BiaoQian> getTop(@Param("top") int top);

}