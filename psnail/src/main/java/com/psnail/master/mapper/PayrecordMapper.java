package com.psnail.master.mapper;

import com.psnail.master.entity.Payrecord;
import com.psnail.master.entity.PayrecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PayrecordMapper {
    int countByExample(PayrecordExample example);

    int deleteByExample(PayrecordExample example);

    int deleteByPrimaryKey(String id);

    int insert(Payrecord record);

    int insertSelective(Payrecord record);

    List<Payrecord> selectByExample(PayrecordExample example);

    Payrecord selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Payrecord record, @Param("example") PayrecordExample example);

    int updateByExample(@Param("record") Payrecord record, @Param("example") PayrecordExample example);

    int updateByPrimaryKeySelective(Payrecord record);

    int updateByPrimaryKey(Payrecord record);
}