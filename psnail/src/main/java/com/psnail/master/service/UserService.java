package com.psnail.master.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.psnail.master.entity.Resource;

public interface UserService {
	Map<String, Object> create(String id, String name, String password, String password_confirm);

	Map<String, Object> login(String email, String password);

	Map<String, Object> newpassword(String id, String password, String password_confirm);

	Map<String, Object> exit();

	Map<String, Object> updatePersonal(String nicheng, MultipartFile file);

	Map<String, Object> clearCache();

	Map<String, Object> uploadPhoto(MultipartFile file);

	Map<String, Object> sendDemand(String title, String content);

	Map<String, Object> sendResource(Resource r);

	Map<String, Object> money(String state, int way, BigDecimal money, String user_id);

	Map<String, Object> chongzhi(String payway, String money);

	Map<String, Object> getPayrecord(String id);

	Map<String, Object> adminAudit(String id, String state);

	Map<String, Object> zanzhu(String money);

	Map<String, Object> shengji();

	Map<String, Object> mycost();

	Map<String, Object> getNumberVip();

	Map<String, Object> vip();

	Map<String, Object> updateVip();

}
