package com.psnail.master.service.Impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.psnail.master.entity.Cost;
import com.psnail.master.entity.CostExample;
import com.psnail.master.entity.CostExample.Criteria;
import com.psnail.master.entity.Demand;
import com.psnail.master.entity.Payrecord;
import com.psnail.master.entity.Resource;
import com.psnail.master.entity.User;
import com.psnail.master.entity.UserExample;
import com.psnail.master.mapper.CostMapper;
import com.psnail.master.mapper.DemandMapper;
import com.psnail.master.mapper.PayrecordMapper;
import com.psnail.master.mapper.ResourceMapper;
import com.psnail.master.mapper.UserMapper;
import com.psnail.master.mapper.dao.DemandDao;
import com.psnail.master.mapper.dao.UserDao;
import com.psnail.master.service.UserService;
import com.psnail.tools.EmailTools;
import com.psnail.tools.IDTools;
import com.psnail.tools.MD5Tools;
import com.psnail.tools.UploadFileTools;

@Service
public class UserServiceImpl implements UserService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private DemandMapper demandMapper;
	@Autowired
	private ResourceMapper resourceMapper;
	@Autowired
	private CostMapper costMapper;
	@Autowired
	private PayrecordMapper payrecordMapper;
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserService userService;
	@Autowired
	private DemandDao demandDao;
	@Autowired
	private EmailTools ets;
	@Autowired
	HttpServletRequest request;

	// 注册邮件发送的地址
	@Value("${user.resource.address}")
	private String userResourceAddress;
	// 站长邮箱
	@Value("${email.code.zhanzhang}")
	private String zhanzhang;
	// 管理员邮箱
	@Value("${email.code.guanliyuan}")
	private String guanliyuan;
	// 充值审查地址
	@Value("${email.chongzhi.audit}")
	private String audit;

	@Override
	public Map<String, Object> create(String email, String name,
			String password, String password_confirm) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (email == null || "".equals(email)) {
			result.put("status", 0);
			result.put("message", "邮箱不能为空！");
		} else if (!ets.emailFormat(email)) {
			result.put("status", 0);
			result.put("message", "邮箱格式不正确！");
		} else if (userDao.selectByEmail(email) != null) {
			result.put("status", 0);
			result.put("message", "该邮箱已注册！请更换邮箱。");
		} else if (name == null || "".equals(name)) {
			result.put("status", 0);
			result.put("message", "昵称不能为空！");
		} else if (name.length() < 3 || name.length() > 20) {
			result.put("status", 0);
			result.put("message", "昵称应该为3-20位之间！");
		} else if (password == null || "".equals(password)) {
			result.put("status", 0);
			result.put("message", "密码不能为空！");
		} else if (password.length() < 6 || password.length() > 20) {
			result.put("status", 0);
			result.put("message", "密码应该为6-20位之间！");
		} else if (!password.equals(password_confirm)) {
			result.put("status", 0);
			result.put("message", "两次输入密码不一致！");
		} else {
			User user = new User();
			user.setId(IDTools.createId());
			user.setEmail(email);
			user.setName(name);
			user.setPassword(MD5Tools.create(password));
			user.setPhoto((new Random().nextInt(7) + 1) + ".jpg");
			user.setMoney(new BigDecimal("0.00"));
			user.setAuthor(0);
			user.setVip(0);
			user.setZanzhu(0);
			user.setState(1);
			user.setTime(new Date());
			int row = userMapper.insert(user);
			if (row > 0) {
				result.put("status", 1);
				result.put("message", "账号注册成功！欢迎登录。");
			} else {
				result.put("status", 0);
				result.put("message", "账号注册失败！请联系站长。");
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> login(String email, String password) {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userDao.selectByEmail(email);
		if (email == null || "".equals(email)) {
			result.put("status", 0);
			result.put("message", "请输入邮箱！");
		} else if (password == null || "".equals(password)) {
			result.put("status", 0);
			result.put("message", "请输入密码！");
		} else if (user == null) {
			result.put("status", 0);
			result.put("message", "邮箱不正确！");
		} else if (!MD5Tools.create(password).equals(user.getPassword())) {
			result.put("status", 0);
			result.put("message", "密码不正确！");
		} else if (user.getState() == 0) {
			result.put("status", 0);
			result.put("message", "账号已禁用！");
		} else {
			HttpSession session = request.getSession(true);
			session.setAttribute("user", user);
			result.put("status", 1);
			result.put("message", "登录成功。");
		}
		return result;
	}

	@Override
	public Map<String, Object> newpassword(String id, String password,
			String password_confirm) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (id == null || "".equals(id)) {
			result.put("status", 0);
			result.put("message", "验证id为空！");
		} else if (password == null || "".equals(password)) {
			result.put("status", 0);
			result.put("message", "密码不能为空！");
		} else if (password.length() < 6 || password.length() > 20) {
			result.put("status", 0);
			result.put("message", "密码应该为6-20位之间！");
		} else if (!password.equals(password_confirm)) {
			result.put("status", 0);
			result.put("message", "两次输入密码不一致！");
		} else {
			User user = new User();
			user.setId(id);
			user.setPassword(MD5Tools.create(password));
			int row = userMapper.updateByPrimaryKeySelective(user);
			if (row > 0) {
				result.put("status", 1);
				result.put("message", "密码修改成功！欢迎登录。");
			} else {
				result.put("status", 0);
				result.put("message", "密码修改失败！请联系站长。");
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> exit() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		session.removeAttribute("user");
		result.put("status", 1);
		result.put("message", "退出账户成功。");
		return result;
	}

	@Override
	public Map<String, Object> updatePersonal(String nicheng, MultipartFile file) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		if (nicheng == null || "".equals(nicheng)) {
			result.put("status", 0);
			result.put("message", "昵称不能为空！");
		} else {
			User user = new User();
			user.setId(((User) session.getAttribute("user")).getId());
			user.setName(nicheng);
			result = UploadFileTools.uploadUserPhoto(file,
					((User) session.getAttribute("user")).getEmail(),
					userResourceAddress);
			if ("1".equals(result.get("status").toString())) {// 图片上传成功
				user.setPhoto((String) result.get("message"));
			}
			int row = userMapper.updateByPrimaryKeySelective(user);
			if (row > 0) {
				result.put("status", 1);
				result.put("message", "个人信息更新成功！返回清除缓存即可。");
				user = userDao.selectByEmail(((User) session
						.getAttribute("user")).getEmail());
				session.removeAttribute("user");
				session.setAttribute("user", user);
			} else {
				result.put("status", 0);
				result.put("message", "个人信息更新失败！请联系站长。");
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> clearCache() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = userDao.selectByEmail(((User) session.getAttribute("user"))
				.getEmail());
		session.removeAttribute("user");
		session.setAttribute("user", user);
		result.put("status", 1);
		result.put("message", "清除缓存成功。");
		return result;
	}

	@Override
	public Map<String, Object> uploadPhoto(MultipartFile file) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else {
			result = UploadFileTools.uploadPhoto(file, user.getEmail(),
					userResourceAddress);
		}
		return result;
	}

	@Override
	public Map<String, Object> sendDemand(String title, String content) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (title == null || "".equals(title)) {
			result.put("status", 0);
			result.put("message", "标题不能为空！");
		} else if (content == null || "".equals(content)) {
			result.put("status", 0);
			result.put("message", "内容不能为空！");
		} else if (content.length() > 10000) {
			result.put("status", 0);
			result.put("message", "内容不能超过1万个字符！");
		} else if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else {
			Demand demand = new Demand();
			demand.setId(IDTools.createId());
			demand.setTitle(title);
			demand.setContent(content);
			demand.setState(1);
			demand.setUserId(user.getId());
			demand.setTime(new Timestamp(System.currentTimeMillis()));
			int row = demandMapper.insert(demand);
			if (row > 0) {
				result.put("status", 1);
				result.put("message", "需求发布成功。");
			} else {
				result.put("status", 0);
				result.put("message", "发布需求失败！请联系站长。");
			}
			/*// 获取该用户本月发布的需求次数
			int count = demandDao.countMonthByUser_id(user.getId());
			if ((user.getVip() == 1 && count < 3)
					|| (user.getZanzhu() == 1 && count < 2)
					|| (user.getVip() == 0 && user.getZanzhu() == 0 && count < 1)) {
				int row = demandMapper.insert(demand);
				if (row > 0) {
					result.put("status", 1);
					result.put("message", "需求发布成功。");
				} else {
					result.put("status", 0);
					result.put("message", "发布需求失败！请联系站长。");
				}
			} else {
				result.put("status", 0);
				result.put("message", "你本月已发布" + count + "次需求！无法发布新需求。");
			}*/
		}
		return result;
	}

	@Override
	public Map<String, Object> sendResource(Resource r) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (r.getTitle() == null || "".equals(r.getTitle())) {
			result.put("status", 0);
			result.put("message", "标题不能为空！");
		} else if (r.getContent() == null || "".equals(r.getContent())) {
			result.put("status", 0);
			result.put("message", "内容不能为空！");
		} else if (r.getContent().length() > 10000) {
			result.put("status", 0);
			result.put("message", "内容不能超过1万个字符！");
		} else if (r.getLabel() == null || r.getLabel() == "") {
			result.put("status", 0);
			result.put("message", "采用技术不能为空！");
		} else if (r.getBackground() == null || r.getBackground() == "") {
			result.put("status", 0);
			result.put("message", "背景图不能为空！");
		} else if (r.getAddress() == null || r.getAddress() == "") {
			result.put("status", 0);
			result.put("message", "演示地址不能为空！");
		} else if ((r.getGithub() == null || r.getGithub() == "")
				&& (r.getGit() == null || r.getGit() == "")
				&& (r.getBaiduyun() == null || r.getBaiduyun() == "")) {
			result.put("status", 0);
			result.put("message", "下载地址不能为空！github、git、百度云三者中必须有一个可正常下载地址");
		} else if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else if (user.getAuthor() == 0) {
			result.put("status", 0);
			result.put("message", "不是源码作者！");
		} else {
			r.setId(IDTools.createId());
			r.setUserId(user.getId());
			r.setTime(new Timestamp(System.currentTimeMillis()));
			int row = resourceMapper.insert(r);
			if (row > 0) {
				result.put("status", 1);
				result.put("message", "源码发布成功。");
			} else {
				result.put("status", 0);
				result.put("message", "发布源码失败！请联系站长。");
			}

		}
		return result;
	}

	@Override
	public Map<String, Object> money(String state, int way, BigDecimal money,
			String user_id) {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userMapper.selectByPrimaryKey(user_id);
		if (user == null) {
			result.put("status", 0);
			result.put("message", "用户不存在！");
		} else {
			BigDecimal u_money = user.getMoney();
			if ("+".equals(state)) {// 账户加钱
				user.setMoney(u_money.add(money));
				int row = userMapper.updateByPrimaryKey(user);
				if (row > 0) {// 加钱成功
					// 添加一条消费记录
					Cost c = new Cost();
					c.setId(user.getId());
					c.setWay(way);
					c.setAmount(money);
					c.setBalance(u_money.add(money));
					c.setTime(new Date());
					row = costMapper.insert(c);
					if (row > 0) {
						result.put("status", 1);
						result.put("message", "收款成功，可以继续业务逻辑处理！");
					} else {
						result.put("status", 0);
						result.put("message", "记录收款明细失败，进而导致此次业务操作失败！请联系站长。");
					}
				} else {
					result.put("status", 0);
					result.put("message", "收款失败，进而导致此次业务操作失败！请联系站长。");
				}
			} else if ("-".equals(state)) {// 账户扣钱
				if (u_money.compareTo(money) == -1) {// 钱包余额不够
					result.put("status", 0);
					result.put("message", "钱包余额不足￥" + money + "！请充值。");
				} else {
					user.setMoney(u_money.subtract(money));
					int row = userMapper.updateByPrimaryKey(user);
					if (row > 0) {// 扣钱成功
						// 添加一条消费记录
						Cost c = new Cost();
						c.setId(user.getId());
						c.setWay(way);
						c.setAmount(money);
						c.setBalance(u_money.subtract(money));
						c.setTime(new Date());
						row = costMapper.insert(c);
						if (row > 0) {
							result.put("status", 1);
							result.put("message", "扣款成功，可以继续业务逻辑处理！");
						} else {
							result.put("status", 0);
							result.put("message",
									"记录扣款明细失败，进而导致此次业务操作失败！请联系站长。");
						}
					} else {
						result.put("status", 0);
						result.put("message", "扣款失败，进而导致此次业务操作失败！请联系站长。");
					}
				}
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> chongzhi(String payway, String money) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else {
			if ("weipay".equals(payway)) {
				payway = "微信支付";
			} else if ("alipay".equals(payway)) {
				payway = "支付宝支付";
			} else {
				payway = "不明确支付方式";
			}
			Payrecord pr = new Payrecord();
			String id = IDTools.createId();
			pr.setId(id);
			pr.setName(user.getName());
			pr.setEmail(user.getEmail());
			pr.setPayway(payway);
			pr.setMoney(new BigDecimal(money));
			pr.setState(0);
			pr.setAdmin("");
			pr.setTime(new Date());
			int row = payrecordMapper.insert(pr);
			if (row > 0) {
				String subject = "[蜗牛库]有人要充值，请尽快确认充值。";
				String content = "用户昵称：" + user.getName();
				content += "<br/>用户邮箱：" + user.getEmail();
				content += "<br/>转账方式：" + payway;
				content += "<br/>转账金额：" + money;
				content += "<br/><br/>请点击下面的链接去充值：<br/>";
				content += audit + "?id=" + id;
				try {
					ets.sendHtmlMail(zhanzhang + guanliyuan, zhanzhang
							+ guanliyuan, subject, content, null);
				} catch (Exception e) {
					logger.error("UserServiceImpl/chongzhi()发送邮件出现异常");
				}
				result.put("status", 1);
				result.put("message", "通知邮件发送成功！请耐心等待。");
			} else {
				result.put("status", 0);
				result.put("message", "通知邮件记录失败！请联系站长。");
			}

		}
		return result;
	}

	@Override
	public Map<String, Object> getPayrecord(String id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Payrecord pr = payrecordMapper.selectByPrimaryKey(id);
		if (pr == null) {
			result.put("status", 0);
			result.put("message", "该通知邮件已不存在！请联系站长。");
		} else {
			result.put("status", 1);
			result.put("message", pr);
			result.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(pr.getTime()));
		}
		return result;
	}

	@Override
	@Transactional
	public Map<String, Object> adminAudit(String id, String state) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else if (!zhanzhang.contains(user.getEmail())
				&& !guanliyuan.contains(user.getEmail())) {
			result.put("status", 0);
			result.put("message", "你没有权限完成充值！");
		} else {
			Payrecord pr = new Payrecord();
			pr = payrecordMapper.selectByPrimaryKey(id);
			if (pr == null) {
				result.put("status", 0);
				result.put("message", "该充值记录已不存在！");
			} else if (pr.getState() != 0) {
				result.put("status", 0);
				result.put("message", "该申请已处理过了！");
			} else if (!"1".equals(state) && !"2".equals(state)) {
				result.put("status", 0);
				result.put("message", "充值状态参数不正确！");
			} else {
				pr.setState(Integer.valueOf(state));
				pr.setAdmin(user.getEmail());
				int row = payrecordMapper.updateByPrimaryKey(pr);
				if (row > 0) {
					String subject = "[蜗牛库]充值结果通知邮件。";
					String content = "您的用户昵称：" + pr.getName();
					content += "<br/>您的用户邮箱：" + pr.getEmail();
					content += "<br/>您的转账方式：" + pr.getPayway();
					content += "<br/>您的转账金额：" + pr.getMoney();
					if (Integer.valueOf(state) == 1) {// 充值
						result = userService.money("+", 1, pr.getMoney(),
								userDao.selectByEmail(pr.getEmail()).getId());
						if ("1".equals(result.get("status").toString())) {
							content += "<br/><br/>充值状态：已完成充值，请清空缓存查看账户。如有疑问，请联系站长。";
							try {
								ets.sendEmail(pr.getEmail(), subject, content);
							} catch (Exception e) {
								logger.error("UserServiceImpl/adminAudit()发送邮件出现异常");
							}
							result.put("status", 1);
							result.put("message", "充值成功！");
						} else {
							result.put("status", 0);
							result.put("message", "用户账号增加金额时数据库操作失败！请联系站长。");
						}
					} else {// 不充值
						content += "<br/><br/>充值状态：未完成充值，我们没有收到您的转款或存在其他问题。如有疑问，请联系站长。";
						try {
							ets.sendEmail(pr.getEmail(), subject, content);
						} catch (Exception e) {
							logger.error("UserServiceImpl/adminAudit()发送邮件出现异常");
						}
						result.put("status", 1);
						result.put("message", "已通过邮件反馈用户！");
					}
				}
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> zanzhu(String state) {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		int s = Integer.valueOf(state);
		BigDecimal money = new BigDecimal("0");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
			return result;
		} else if (s == 1) {
			money = new BigDecimal("4.9");
		} else if (s == 2) {
			money = new BigDecimal("9.9");
		} else if (s == 3) {
			money = new BigDecimal("19.9");
		} else if (s == 4) {
			money = new BigDecimal("29.9");
		} else if (s == 5) {
			money = new BigDecimal("39.9");
		} else if (s == 6) {
			money = new BigDecimal("59.9");
		} else if (s == 7) {
			money = new BigDecimal("99.9");
		} else {
			result.put("status", 0);
			result.put("message", "赞助金额无法识别！");
			return result;
		}
		int size = user.getMoney().compareTo(money);
		if (size >= 0) {
			result = userService.money("-", 3, money, user.getId());
			if ("1".equals(result.get("status").toString())) {
				result.put("status", 1);
				result.put("message", "赞助成功！");
				user = userDao.selectByEmail(user.getEmail());
				session.removeAttribute("user");
				session.setAttribute("user", user);
			}
		} else {
			result.put("status", 0);
			result.put("message", "钱包余额不足！");
		}
		return result;
	}

	@Override
	public Map<String, Object> shengji() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else {
			CostExample example = new CostExample();
			Criteria c = example.createCriteria();
			c.andIdEqualTo(user.getId());
			c.andWayEqualTo(3);
			int row = costMapper.countByExample(example);
			if (row > 0) {
				user.setZanzhu(1);
				row = userMapper.updateByPrimaryKey(user);
				if (row > 0) {
					result.put("status", 1);
					result.put("message", "升级成功！");
				} else {
					result.put("status", 0);
					result.put("message", "升级失败！请联系站长。");
				}
			} else {
				result.put("status", 0);
				result.put("message", "没有查询到您的赞助记录！");
			}
		}

		return result;
	}

	@Override
	public Map<String, Object> mycost() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else {
			CostExample example = new CostExample();
			Criteria c = example.createCriteria();
			c.andIdEqualTo(user.getId());
			List<Cost> costs = costMapper.selectByExample(example);
			String msg = "";
			int i = 1;
			for (Cost cost : costs) {
				msg += "<tr>";
				msg += "<td>" + i + "</td>";
				msg += "<td>"
						+ com.psnail.master.enums.Cost.getCost(cost.getWay())
						+ cost.getAmount() + "</td>";
				msg += "<td>" + cost.getBalance() + "</td>";
				msg += "<td>"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.format(cost.getTime()) + "</td>";
				msg += "</tr>";
				i++;
			}
			result.put("status", 1);
			result.put("message", msg);
		}

		return result;
	}

	@Override
	public Map<String, Object> getNumberVip() {
		Map<String, Object> result = new HashMap<String, Object>();
		UserExample example = new UserExample();
		com.psnail.master.entity.UserExample.Criteria c = example
				.createCriteria();
		c.andVipEqualTo(1);
		int number = userMapper.countByExample(example);
		result.put("number", number);
		return result;
	}

	@Override
	@Transactional
	public Map<String, Object> vip() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			result.put("status", 0);
			result.put("message", "尚未登录！");
		} else if (user.getVip() == 1) {
			result.put("status", 0);
			result.put("message", "您已经是VIP用户了！");
		} else {
			UserExample example = new UserExample();
			com.psnail.master.entity.UserExample.Criteria c = example
					.createCriteria();
			c.andVipEqualTo(1);
			int number = userMapper.countByExample(example);
			int money = 10 + ((number / 20)) * 5;
			logger.info("userServiceImpl/vip()当前VIP定价为：" + money);
			result = userService.money("-", 4,
					new BigDecimal(String.valueOf(money)), user.getId());
		}

		return result;
	}

	@Override
	public Map<String, Object> updateVip() {
		Map<String, Object> result = new HashMap<String, Object>();
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		user = userDao.selectByEmail(user.getEmail());
		user.setVip(1);
		int row = userMapper.updateByPrimaryKey(user);
		if (row > 0) {
			result.put("status", 1);
			result.put("message", "VIP开通成功！");
			user = userDao.selectByEmail(user.getEmail());
			session.removeAttribute("user");
			session.setAttribute("user", user);
		} else {
			result.put("status", 0);
			result.put("message", "VIP开通失败！请联系站长");
		}
		return result;
	}

}